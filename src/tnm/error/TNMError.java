package tnm.error;

import tnm.javalib.TNMObject;

public class TNMError extends TNMObject implements ITNMError
{
    protected int code;
    protected String message;
    
    public TNMError(int code)
    {
        super();
        this.code=code;
        switch(code)
        {
            case 1:
                message="Init Connection Error";
                break;
            case 2:
                message="Parser Package Error";
                break;
            case 3:
                message="Decode Error";
                break;
            case 4:
                message="Send Message Error";
                break;
            case 5:
                message="Not Permission Response Delay Time";
                break;
            case 6:
                message="Response Id Exist";
                break;
            case 7:
                message="Not Permission Delta Id";
                break;
            case 8:
                message="Not Permission Delta Response Millis";
                break;
            case 9:
                message="Not Permission Average Response";
                break;
            case 10:
                message="Routing Error";
                break;
            case 11:
                message="Not Found Code Error";
                break;
            case 12:
                message="Create Key Error";
                break;
            case 13:
                message="Decrypt Message Error";
                break;
            case 14:
                message="Encrypt Message Error";
                break;
            case 15:
                message="Not Support Decrypt RSA";
                break;
            case 16:
                message="CMD Invalidate";
                break;
        }
    }

    public int getCode() 
    {
        return code;
    }

    public String getMessage() 
    {
        return message;
    }   
}
