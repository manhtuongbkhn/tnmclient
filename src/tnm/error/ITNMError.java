package tnm.error;

import tnm.javalib.ITNMObject;

public interface ITNMError extends ITNMObject
{
    public int getCode();
    
    public String getMessage();
}
