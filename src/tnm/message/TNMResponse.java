package tnm.message;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import tnm.javalib.TNMCode;
import tnm.javalib.TNMKJS;

public class TNMResponse extends TNMSMessage implements ITNMResponse
{   
    public TNMResponse(JsonObject content)
    {
        super(content);
    }
    
    public TNMResponse(JsonObject content,TNMCode code)
    {
        super(content,code);
    }
    
    public TNMResponse(String prefixCmd,JsonObject data)
    {
        super(prefixCmd,data);
    }
    
    public TNMResponse(String prefixCmd,JsonObject data,TNMCode code)
    {
        super(prefixCmd,data,code);
    }
    
    @Override
    public String getSuffixCmd()
    {
        return "_response";
    }
    
    @Override
    public int getRequestId() 
    {
        JsonElement jsonElement=content.getAsJsonObject(TNMKJS.EXTENSION).get(TNMKJS.REQUEST_ID);
        if(jsonElement!=null)
            return jsonElement.getAsInt();
        else
            return 0;
    }
}
