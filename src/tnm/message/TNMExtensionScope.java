 package tnm.message;

public enum TNMExtensionScope 
{
    SYSTEM,SERVER,DEVELOP;
    
    public static TNMExtensionScope create(String scope)
    {
        switch(scope)
        {
            case "server":
            case "SERVER":    
                return TNMExtensionScope.SERVER;
            case "system":
            case "SYSTEM":
                return TNMExtensionScope.SYSTEM;
            case "develop":
            case "DEVELOP":
                return TNMExtensionScope.DEVELOP;
        }
        return null;
    }
}
