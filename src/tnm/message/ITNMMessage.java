package tnm.message;

import com.google.gson.JsonObject;
import java.util.Date;
import tnm.javalib.ITNMObject;
import tnm.javalib.TNMCode;

public interface ITNMMessage extends ITNMObject
{
    public int getId();
    
    public Date getTime();
    
    public String getCmd();
    
    public String getPrefixCmd();
    
    public String getSuffixCmd();

    public JsonObject getData();
    
    public TNMCode getCode();
    
    public JsonObject getContent();
}
