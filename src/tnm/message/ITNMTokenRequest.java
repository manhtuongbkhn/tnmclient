package tnm.message;

public interface ITNMTokenRequest extends ITNMRequest
{
    public void receivedToken(String token);
}
