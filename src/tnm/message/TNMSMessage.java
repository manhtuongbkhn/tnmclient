package tnm.message;

import com.google.gson.JsonObject;
import tnm.javalib.TNMCode;

abstract public class TNMSMessage extends TNMMessage implements ITNMSMessage
{            
    protected boolean bHandled=false;
        
    protected TNMSMessage(JsonObject content)
    {
        super(content);
    }
    
    protected TNMSMessage(JsonObject content,TNMCode code)
    {
        super(content,code);
    }
    
    protected TNMSMessage(String prefixCmd,JsonObject data)
    {
        super(prefixCmd,data);
    } 
    
    protected TNMSMessage(String prefixCmd,JsonObject data,TNMCode code)
    {
        super(prefixCmd,data,code);
    }
    
    @Override
    public boolean isHandled()
    {
        return bHandled;
    }
    
    @Override
    public void handled()
    {
        this.bHandled=true;
    }
}
