package tnm.message;

import com.google.gson.JsonObject;
import java.util.Date;
import tnm.javalib.TNMCode;
import tnm.javalib.TNMObject;
import tnm.javalib.TNMKJS;

abstract public class TNMMessage extends TNMObject implements ITNMMessage
{
    protected TNMCode code;
    protected JsonObject content;
    
    public TNMMessage(JsonObject content)
    {
        this(content,TNMCode.NONE);
    }
    
    public TNMMessage(JsonObject content,TNMCode code)
    {
        this.content=content;
        this.code=code;
    }
    
    public TNMMessage(String prefixCmd,JsonObject data)
    {
        this(prefixCmd,data,TNMCode.NONE);
    } 
    
    public TNMMessage(String prefixCmd,JsonObject data,TNMCode code)
    {
        content=new JsonObject();
        JsonObject extension=new JsonObject();
        extension.addProperty(TNMKJS.CMD,prefixCmd.concat(getSuffixCmd()));
        content.add(TNMKJS.EXTENSION,extension);
        content. add(TNMKJS.DATA,data);
        this.code=code;
    } 
    
    @Override
    public int getId()
    {
        return content.get(TNMKJS.ID).getAsInt(); 
    }
    
    @Override
    public Date getTime()
    {
        long longTime=content.get(TNMKJS.TIME).getAsInt();
        return new Date(longTime);
    }
    
    @Override
    public String getCmd() 
    {
        JsonObject extension=content.get(TNMKJS.EXTENSION).getAsJsonObject();
        return extension.get(TNMKJS.CMD).getAsString();
    }

    @Override
    public String getPrefixCmd() 
    {
        try
        {
            String cmd=getCmd();
            return cmd.substring(0,cmd.length()-getSuffixCmd().length());
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    
    @Override
    abstract public String getSuffixCmd();
    
    @Override
    public JsonObject getData() 
    {
        return content.get(TNMKJS.DATA).getAsJsonObject();
    }
    
    @Override
    public JsonObject getContent()
    {
        return content;
    }
    
    @Override
    public TNMCode getCode()
    {
        return code;
    }
}
