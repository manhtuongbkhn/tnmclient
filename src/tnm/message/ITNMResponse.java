package tnm.message;

public interface ITNMResponse extends ITNMSMessage
{
    public int getRequestId();
}
