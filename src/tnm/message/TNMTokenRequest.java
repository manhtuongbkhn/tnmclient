package tnm.message;

import com.google.gson.JsonObject;
import tnm.config.TNMCMDRQ;

abstract public class TNMTokenRequest extends TNMRequest implements ITNMTokenRequest
{
    public TNMTokenRequest()
    {
        super(TNMCMDRQ.TNM_CREATE_TOKEN,new JsonObject(),TNMExtensionScope.SYSTEM);
    }  
    
    abstract public void receivedToken(String token);
}
