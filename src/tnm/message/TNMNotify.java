package tnm.message;

import com.google.gson.JsonObject;
import tnm.javalib.TNMCode;

public class TNMNotify extends TNMSMessage implements ITNMNotify
{
    public TNMNotify(JsonObject content)
    {
        super(content);
    }
    
    public TNMNotify(JsonObject content,TNMCode code)
    {
        super(content,code);
    }
    
    public TNMNotify(String prefixCmd,JsonObject data)
    {
        super(prefixCmd,data);
    }
    
    public TNMNotify(String prefixCmd,JsonObject data,TNMCode code)
    {
        super(prefixCmd,data,code);
    }

    @Override
    public String getSuffixCmd() 
    {
        return "_notify";
    }
}
