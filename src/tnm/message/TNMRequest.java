package tnm.message;

import com.google.gson.JsonObject;
import java.util.Date;
import tnm.config.TNMConfig;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.TNMArrayList;
import tnm.javalib.TNMCode;
import tnm.javalib.TNMKJS;

public class TNMRequest extends TNMMessage implements ITNMRequest
{
    protected boolean bSend=false;
    protected boolean bResponsed=false;
    protected int waitResponseTimeMillis=TNMConfig.DEFAULT_WAIT_RESPONSE_TIME_MILLIS;
    protected ITNMArrayList<ITNMRequestHandler> requestHandlers=new TNMArrayList<ITNMRequestHandler>();
    
    public TNMRequest(JsonObject content)
    {
        super(content); 
    }
    
    public TNMRequest(JsonObject content,TNMCode code)
    {
        super(content,code);
    }
    
    public TNMRequest(String prefixCmd,JsonObject data)
    {
        this(prefixCmd,data,TNMExtensionScope.DEVELOP,TNMCode.NONE);
    }
    
    public TNMRequest(String prefixCmd,JsonObject data,TNMCode code)
    {
        this(prefixCmd,data,TNMExtensionScope.DEVELOP,code);
    }

    public TNMRequest(String prefixCmd,JsonObject data,TNMExtensionScope extensionScope)
    {
        this(prefixCmd,data,extensionScope,TNMCode.NONE);
    }
    
    public TNMRequest (String prefixCmd,JsonObject data,TNMExtensionScope extensionScope,TNMCode code)
    {
        super(null,code);
        content=new JsonObject();    
        content.add(TNMKJS.DATA,data);
        JsonObject extension=new JsonObject();
        extension.addProperty(TNMKJS.CMD,prefixCmd.concat(getSuffixCmd()));
        content.add(TNMKJS.EXTENSION,extension);
        switch(extensionScope)
        {
            case SYSTEM:
                extension.addProperty(TNMKJS.SCOPE,"SYSTEM");
                break;
            case SERVER:
                extension.addProperty(TNMKJS.SCOPE,"SERVER");
                break;
            case DEVELOP:
                extension.addProperty(TNMKJS.SCOPE,"DEVELOP");
                break;
        }
    }
    
    @Override
    public void setId(int id)
    {
        content.addProperty(TNMKJS.ID,id);
    }
    
    @Override
    public void setTime()
    {
        content.addProperty(TNMKJS.TIME,new Date().getTime());
    }
    
    @Override
    public TNMExtensionScope getExtensionScope() 
    {
        JsonObject extension=content.get(TNMKJS.EXTENSION).getAsJsonObject();
        String scope=extension.get(TNMKJS.SCOPE).getAsString();
        return TNMExtensionScope.create(scope);
    }
    
    @Override
    public String getSuffixCmd() 
    {
        return "_request";
    }
    
    public int getWaitResponseTimeMillis()
    {
        return waitResponseTimeMillis;
    }
    
    public void setWaitResponseTimeMillis(int timeMillis)
    {
        this.waitResponseTimeMillis=timeMillis;
    }
    
    public void addRequestHandler(ITNMRequestHandler requestHandler)
    {
        requestHandlers.add(requestHandler);
    }
    
    public void onSuccess(ITNMResponse response)
    {
        for(int i=0;i<requestHandlers.size();i++)
        {
            ITNMRequestHandler requestHandler=requestHandlers.get(i);
            requestHandler.onRequestSuccess(this,response);
        }
    }
    
    public void onError(int code,String message)
    {
        for(int i=0;i<requestHandlers.size();i++)
        {
            ITNMRequestHandler requestHandler=requestHandlers.get(i);
            requestHandler.onRequestError(this,code,message);
        }
    }
    
    public boolean isSended()
    {
        return bSend;
    }
    
    public void sended()
    {
        this.bSend=true;
    }

    @Override
    public boolean isResponsed() 
    {
        return bResponsed;
    }

    @Override
    public void responsed()
    {
        this.bResponsed=true;
    }
}
