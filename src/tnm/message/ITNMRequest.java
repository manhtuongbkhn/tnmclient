package tnm.message;

import tnm.communicate.ITNMSMessageListener;

public interface ITNMRequest extends ITNMMessage,ITNMSMessageListener
{
    public void setId(int id);
    
    public void setTime();
    
    public TNMExtensionScope getExtensionScope();
    
    public int getWaitResponseTimeMillis();
    
    public void setWaitResponseTimeMillis(int timeMillis);
    
    public void addRequestHandler(ITNMRequestHandler requestHandler);
    
    public void onSuccess(ITNMResponse response);
    
    public void onError(int code,String message);
    
    public boolean isSended();
    
    public void sended();
    
    public boolean isResponsed();
    
    public void responsed();
}
