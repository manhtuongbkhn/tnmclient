package tnm.message;

public interface ITNMSMessage extends ITNMMessage
{
    public boolean isHandled();
    
    public void handled();
}
