package tnm.message;

import tnm.javalib.ITNMObject;

public interface ITNMRequestHandler extends ITNMObject
{
    public void onRequestSuccess(ITNMRequest request,ITNMResponse response);
    
    public void onRequestError(ITNMRequest request,int code,String message);
}
