package tnm;

import tnm.message.ITNMRequest;
import tnm.communicate.ITNMConnection;
import tnm.message.TNMRequest;
import tnm.communicate.TNMJavaConnection;
import tnm.javalib.TNMParams;
import tnm.javalib.ITNMParams;
import tnm.error.TNMError;
import tnm.error.ITNMError;
import com.google.gson.JsonObject;
import tnm.communicate.ITNMConnectionEventHandler;
import tnm.javalib.syncdata.node.ITNMElementNode;
import tnm.javalib.syncdata.node.TNMNodeApi;
import tnm.config.TNMCMDNF;
import tnm.config.TNMCMDRQ;
import tnm.config.TNMConfig;
import tnm.javalib.ITNMDES;
import tnm.javalib.TNMDES;
import tnm.message.TNMExtensionScope;
import tnm.javalib.TNMRSA;
import tnm.message.ITNMNotify;
import tnm.message.ITNMResponse;
import tnm.message.ITNMSMessage;
import tnm.javalib.ITNMHashMap;
import tnm.javalib.ITNMRSA;
import tnm.javalib.ITNMResult;
import tnm.javalib.TNMCode;
import tnm.javalib.TNMHashMap;
import tnm.javalib.TNMKJS;
import tnm.message.ITNMRequestHandler;
import tnm.message.ITNMTokenRequest;

abstract public class TNMClient implements ITNMClient
{
    protected ITNMConnection connection;
    private ITNMHashMap<Integer,ITNMRequest> requests;
    
    public TNMClient(String host,int port,TNMConfig config)
    {
        JsonObject data=new JsonObject();
        data.addProperty(TNMKJS.TYPE,"new_communicator");
        init(host,port,data,config);
    }
    
    public TNMClient(String host,int port,TNMConfig config,int userId,String token)
    {
        JsonObject data=new JsonObject();
        data.addProperty(TNMKJS.TYPE,"add_connection");
        data.addProperty(TNMKJS.USER_ID,userId);
        data.addProperty(TNMKJS.TOKEN,token);
        init(host,port,data,config);
    }
    
    public TNMClient(String host,int port,TNMConfig config,int userId,String token,int postion)
    {
        JsonObject data=new JsonObject();
        data.addProperty(TNMKJS.TYPE,"replace_connection");
        data.addProperty(TNMKJS.USER_ID,userId);
        data.addProperty(TNMKJS.POSITION,postion);
        data.addProperty(TNMKJS.TOKEN,token);
        init(host,port,data,config);
    }
    
    protected void init(String host,int port,JsonObject data,TNMConfig config)
    {
        requests=new TNMHashMap<Integer,ITNMRequest>();
        connection=new TNMJavaConnection(host,port);
        connection.addEventHandler(new ITNMConnectionEventHandler() 
        {
            @Override
            public void onHandshaked(ITNMConnection connection)
            {
                TNMClient.this.connection.handshaked();
            }

            @Override
            public void onDisconnectd(ITNMConnection connection) 
            {
                
            }

            @Override
            public void onError(ITNMConnection connection, int code, ITNMParams params) 
            {
                
            }

            @Override
            public void onSMessage(ITNMConnection connection, ITNMSMessage message) 
            {
                TNMClient.this.routing(message);
            }
        });
        initKey(data);
        ITNMRequest request=new TNMRequest(TNMCMDRQ.TNM_COMMUNICATE,data
                                        ,TNMExtensionScope.SYSTEM,TNMCode.NONE);
        ITNMRequestHandler requestHandler=new ITNMRequestHandler() 
        {
            @Override
            public void onRequestSuccess(ITNMRequest request, ITNMResponse response) 
            {
                JsonObject data=response.getData();
                if(data.get(TNMKJS.SUCCESS).getAsBoolean())
                {
                    onHandshaked();
                }
            }

            @Override
            public void onRequestError(ITNMRequest request, int code, String message) 
            {
                
            }
        };
        request.addRequestHandler(requestHandler);
        sendRequest(request);
        TNMConfig.setDefault(config);
    }
    
    protected void initKey(JsonObject data)
    {
        ITNMDES des=new TNMDES();
        ITNMResult result=des.createKey();
        if(!result.isSuccess())
        {
            ITNMParams params=new TNMParams();
            params.setParam(TNMKJS.CODE,TNMCode.DES);
            error(new TNMError(12),params);
        }
        String desKey=(String) result.getResult();
        connection.getKeyManager().setDESKey(desKey);
        ITNMRSA rsa=new TNMRSA();
        rsa.init(TNMConfig.getDefault().RSA_PUBLICKEY,TNMConfig.getDefault().RSA_PRIVATEKEY);
        result=rsa.encrypt(desKey);
        if(!result.isSuccess())
        {
            ITNMParams params=new TNMParams();
            params.setParam(TNMKJS.CODE,TNMCode.RSA);
            error(new TNMError(13),params);
        }
        String encryptDesKey=(String) result.getResult();
        data.addProperty(TNMKJS.DES,encryptDesKey);
        
    }
    
    public void routing(ITNMSMessage sMessage) 
    {
        try
        {
            String prefixCmd=sMessage.getPrefixCmd();
            JsonObject data=sMessage.getData();
            if(sMessage instanceof ITNMResponse)
            {
                ITNMResponse response=(ITNMResponse) sMessage;
                int requestId=response.getRequestId();
                ITNMRequest request=requests.get(requestId);
                if(request!=null)
                {
                    if(!request.isResponsed())
                    {
                        request.responsed();
                        synchronized(request)
                        {
                            request.notifyAll();
                        }
                        requests.remove(requestId);
                        request.onSuccess(response);
                    }
                }
                else
                    handling(response);
                return ;
            }

            if(sMessage instanceof ITNMNotify)
            {
                ITNMNotify notify=(ITNMNotify) sMessage;
                switch(prefixCmd)
                {
                    case TNMCMDNF.TNM_SYNC_DATA:
                        TNMNodeApi.getDefault().onNewNotify(data);
                    default:
                        handling(notify);
                        return ;
                }
            }
        }
        catch(Exception ex)
        {
            ITNMParams params=new TNMParams();
            params.setParam(TNMKJS.EXCEPTION,ex);
            error(new TNMError(10),params);
        }
    }
    
    public void sendRequest(final ITNMRequest request)
    {
        request.sended();
        connection.send(request);
        requests.put(request.getId(),request);
        Thread thread=new Thread()
        {
            @Override
            public void run()
            {
                synchronized(request)
                {
                    try {request.wait(TNMConfig.PERMISSION_RESPONSE_DELAY_TIME_MILLIS);} 
                    catch (Exception ex) {ex.printStackTrace();}
                }
                if(!request.isResponsed())
                {
                    request.onError(1,"Delay Time Out");
                    requests.remove(request.getId());
                }
            }
        };
        thread.setPriority(TNMConfig.WAIT_RESPONSE_THREAD_PRIORITY);
        thread.start();
    }
    
    public void createToken(ITNMTokenRequest request)
    {
        sendRequest(request);
    }   
    
    public void registerNode(final ITNMElementNode elementNode)
    {
        String token=elementNode.getToken();
        if(elementNode.getId()<=0)
        {
            elementNode.error(2,"Node Id Invalildate",new TNMParams());
            return ;
        }
        if(token==null)
        {
            elementNode.error(3,"Node Token Null",new TNMParams());
            return ;
        }
        
        JsonObject requestData=new JsonObject();
        requestData.addProperty(TNMKJS.NODE_TYPE,elementNode.getType());
        requestData.addProperty(TNMKJS.NODE_ID,elementNode.getId());
        requestData.addProperty(TNMKJS.TOKEN,token);
        ITNMRequest request=new TNMRequest(TNMCMDRQ.TNM_REGISTER_NODE
                                            ,requestData,TNMExtensionScope.SYSTEM);
        ITNMRequestHandler requestHandler=new ITNMRequestHandler() 
        {
            @Override
            public void onRequestSuccess(ITNMRequest request, ITNMResponse response)
            {
                JsonObject responseData=response.getData();
                if(!responseData.get(TNMKJS.SUCCESS).getAsBoolean())
                {
                    TNMNodeApi.getDefault().removeNode(elementNode);
                    elementNode.error(4,"Can't Register Node",new TNMParams());
                }
            }

            @Override
            public void onRequestError(ITNMRequest request, int code, String message)
            {
                TNMNodeApi.getDefault().removeNode(elementNode);
                elementNode.error(5, message,new TNMParams());
            }
        };
        request.addRequestHandler(requestHandler);
        TNMNodeApi.getDefault().addNode(elementNode);
        sendRequest(request);
    }
    
    public void unregisterNode(ITNMElementNode elementNode)
    {
        long nodeId=elementNode.getId();
        if(nodeId>0)
        {
            TNMNodeApi.getDefault().removeNode(elementNode);
            JsonObject data=new JsonObject();
            data.addProperty(TNMKJS.NODE_TYPE,elementNode.getType());
            data.addProperty(TNMKJS.NODE_ID,nodeId);
            ITNMRequest request=new TNMRequest(TNMCMDRQ.TNM_UNREGISTER_NODE
                                                ,data,TNMExtensionScope.SYSTEM);
            ITNMRequestHandler requestHandler=new ITNMRequestHandler() 
            {
                @Override
                public void onRequestSuccess(ITNMRequest request, ITNMResponse response) 
                {
                    
                }

                @Override
                public void onRequestError(ITNMRequest request, int code, String message) 
                {
                    
                }
            };
            request.addRequestHandler(requestHandler);
            sendRequest(request);
        }
    }
    
    abstract public void onHandshaked();
    
    abstract public boolean handling(ITNMResponse response);
    
    abstract public void handling(ITNMNotify notify);
    
    abstract protected void error(ITNMError error,ITNMParams params);
}
