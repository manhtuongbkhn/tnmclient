package tnm;

import tnm.javalib.syncdata.node.ITNMElementNode;
import tnm.message.ITNMRequest;
import tnm.message.ITNMSMessage;
import tnm.message.ITNMTokenRequest;

public interface ITNMClient 
{
    public void sendRequest(ITNMRequest request);
    
    public void onHandshaked();
    
    public void handling(ITNMSMessage sMessage);
    
    public void createToken(ITNMTokenRequest request);
    
    public void registerNode(ITNMElementNode elementNode);
    
    public void unregisterNode(ITNMElementNode elementNode);
}
