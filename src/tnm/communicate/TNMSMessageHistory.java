package tnm.communicate;

import tnm.javalib.ITNMResult;
import tnm.javalib.TNMResult;
import java.util.Date;
import tnm.javalib.TNMObject;
import tnm.javalib.TNMArrayList;
import tnm.message.ITNMSMessage;

public class TNMSMessageHistory extends TNMObject implements ITNMSMessageHistory
{
    private TNMArrayList<ITNMSMessage> sMessages;
    
    public TNMSMessageHistory()
    {
        sMessages=new TNMArrayList<ITNMSMessage>();
    }
    
    public ITNMResult addSMessage(ITNMSMessage sMessage)
    {
        Date serverTime=sMessage.getTime();
        Date clientTime=sMessage.getTime();
        long detltaTime=clientTime.getTime()-serverTime.getTime();
        if(Math.abs(detltaTime)>TNMCommunicateConfig
                                        .PERMISSION_RESPONSE_DELAY_TIME_MILLIS)
            return new TNMResult(1,"Not Permission Response Delay Time",null);
        int id=sMessage.getId();
        if(checkIdExist(id))
            return new TNMResult(2,"Response Id Exist",null);
        
        ITNMSMessage lastSMessage=getLastSMessage();
        if(lastSMessage!=null)
        {
            if(Math.abs(id-lastSMessage.getId())>TNMCommunicateConfig.PERMISSION_DELTA_RESPONSE_ID)
                return new TNMResult(3,"Not Permission Delta Id",null);
            if(serverTime.getTime()-lastSMessage.getTime().getTime()
                            <TNMCommunicateConfig.PERMISSION_DELTA_RESPONSE_MILLIS)
                return new TNMResult(4,"Not Permission Delta Response Millis",null);
            else
            {
                addSMessageToArr(sMessage);
                int averageResponseInSecond=averageSMessageInSecond();
                if(averageResponseInSecond<0||averageResponseInSecond
                    >TNMCommunicateConfig.PERMISSION_AVERAGE_RESPONSE_IN_SECOND)
                    return new TNMResult(5,"Not Permission Average Response",null);
                else
                    return new TNMResult(0,"Success",sMessage);
            }
        }
        else
        {
            addSMessageToArr(sMessage);
            return new TNMResult(0,"Success",sMessage);
        }
    }
    
    private boolean isFull()
    {
        return sMessages.size()>TNMCommunicateConfig.RESPONSE_HISTORY_COUNT;
    }
    
    private boolean addSMessageToArr(ITNMSMessage sMessage)
    {
        int index=0;
        while(index<sMessages.size())
        {
            ITNMSMessage currentSMessage=sMessages.get(index);
            int currentSMessageId=currentSMessage.getId();
            if(currentSMessageId>sMessage.getId())
                break;
            index++;
        }
        sMessages.add(index,sMessage);
        if(isFull())
            sMessages.remove(0);
        return true;
    }
    
    private int averageSMessageInSecond()
    {
        if(isFull())
        {
            long lDeltaTimeMillis=getLastSMessage().getTime().getTime()
                                        -getFirstMessage().getTime().getTime();
            int deltaTimeMillis=(int) lDeltaTimeMillis;
            return 20*1000/deltaTimeMillis;
        }
        return 0;
    }
    
    private ITNMSMessage getFirstMessage()
    {
        if(sMessages.size()>0)
            return sMessages.get(0);
        else
            return null;
    }
    
    private ITNMSMessage getLastSMessage()
    {
        if(sMessages.size()>0)
            return sMessages.get(sMessages.size()-1);
        else
            return null;
    }
    
    private boolean checkIdExist(int id)
    {
        ITNMSMessage sMessage=getSMessageById(id);
        if(sMessage==null)
            return false;
        else
            return true;
    }
    
    public ITNMSMessage getSMessageById(int id)
    {
        for(int i=0;i<sMessages.size();i++)
        {
            ITNMSMessage sMessage=sMessages.get(i);
            if(sMessage.getId()==id)
                return sMessage;
        }
        return null;
    }
}

