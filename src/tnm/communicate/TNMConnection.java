package tnm.communicate;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMDES;
import tnm.javalib.ITNMParams;
import tnm.javalib.ITNMRSA;
import tnm.javalib.ITNMResult;
import tnm.javalib.TNMArrayList;
import tnm.javalib.TNMCode;
import tnm.javalib.TNMDES;
import tnm.javalib.TNMKJS;
import tnm.javalib.TNMObject;
import tnm.javalib.TNMParams;
import tnm.javalib.TNMRSA;
import tnm.message.ITNMRequest;
import tnm.message.ITNMSMessage;
import tnm.message.TNMNotify;
import tnm.message.TNMResponse;

abstract public class TNMConnection extends TNMObject implements ITNMConnection
{
    protected boolean bHandshaked=false;
    protected ITNMRequestHistory requestHistory;
    protected ITNMSMessageHistory sMessageHistory;
    protected ITNMKeyManager keyManager;
    protected ITNMArrayList<ITNMConnectionEventHandler> eventHandlers;
    protected ITNMParams propeties;
    
    public TNMConnection()
    {
        this.propeties=new TNMParams();
        this.sMessageHistory=new TNMSMessageHistory();
        this.requestHistory=new TNMRequestHistory();
        this.keyManager=new TNMKeyManager();
        this.eventHandlers=new TNMArrayList<ITNMConnectionEventHandler>();
    }
    
    protected void onMessageFromSocket(String dataStr)
    {
        if(TNMCommunicateConfig.getDefault().SOCKET_DEBUG_ENABLE)
        {
            System.out.println("Receive:"+dataStr);
        }
        Gson gson=new Gson();
        JsonObject packageJson=gson.fromJson(dataStr,JsonObject.class);
        newMessageEvent(packageJson);
    }
    
    private void newMessageEvent(final JsonObject packageJson)
    {
        JsonObject content=null;
        String strContent;
        TNMCode code=TNMCode.valueOf(packageJson.get(TNMKJS.CODE).getAsString());
        Gson gson=new Gson();
        ITNMParams params;
        switch(code)
        {
            case NONE:
                content=packageJson.get(TNMKJS.CONTENT).getAsJsonObject();
                break;
            case DES:
                strContent=packageJson.get(TNMKJS.CONTENT).getAsString();
                ITNMDES des=new TNMDES();
                ITNMResult result=des.decrypt(keyManager.getDESKey(),strContent);
                if(!result.isSuccess())
                {
                    params=new TNMParams();
                    params.setParam(TNMKJS.CODE,TNMCode.DES);
                    params.setParam(TNMKJS.CONNECTION,this);
                    onError(13,params);
                }
                content=gson.fromJson((String)result.getResult(),JsonObject.class);
                break;
            case RSA:
                params=new TNMParams();
                params.setParam(TNMKJS.CODE,TNMCode.RSA);
                params.setParam(TNMKJS.CONNECTION,this);
                onError(15,params);
                break;
            default:
                params=new TNMParams();
                params.setParam(TNMKJS.CONNECTION,this);
                onError(11,params);
                break;

        }
        String cmd=content.getAsJsonObject(TNMKJS.EXTENSION)
                                    .get(TNMKJS.CMD).getAsString();
        if(!(isResponse(cmd)||isNotify(cmd)))
        {
            params=new TNMParams();
            params.setParam(TNMKJS.CONNECTION,this);
            onError(16,params);
        }
        ITNMSMessage sMessage=null;
        if(isResponse(cmd))
            sMessage=new TNMResponse(content,code);
        if(isNotify(cmd))
            sMessage=new TNMNotify(content,code);
        ITNMResult result=sMessageHistory.addSMessage(sMessage);
        switch(result.getCode())
        {
            case 0:
                break;
            default:
                onError(4+result.getCode(),new TNMParams());
                break;
        }
        transitClient(sMessage);
    }
    
    private void transitClient(ITNMSMessage sMessage)
    {
        int sMessageId=sMessage.getId();
        if(sMessageId!=1)
        {
            ITNMSMessage beforeSMessage=sMessageHistory.getSMessageById(sMessageId-1);
            if(beforeSMessage==null)
            { 
                synchronized(sMessage)
                {
                    try{sMessage.wait(TNMCommunicateConfig.DELAY_SMESSAGE_TIME_MILLIS);}
                    catch(Exception ex){}
                }
                if(!sMessage.isHandled())
                    handling(sMessage);
            }
            else
            {
                if(beforeSMessage.isHandled())
                    handling(sMessage);
                else
                {
                    synchronized(sMessage)
                    {
                        try{sMessage.wait(TNMCommunicateConfig.DELAY_SMESSAGE_TIME_MILLIS);}
                        catch(Exception ex){}
                    }
                    if(!sMessage.isHandled())
                        handling(sMessage);
                }
            }
        }
        else
            handling(sMessage);
    }
    
    private void handling(ITNMSMessage sMessage)
    {
        sMessage.handled();
        for(int i=0;i<eventHandlers.size();i++)
        {
            ITNMConnectionEventHandler eventHandler=eventHandlers.get(i);
            eventHandler.onSMessage(this,sMessage);
        }
        int sMessageId=sMessage.getId();
        ITNMSMessage afterSMessage=sMessageHistory.getSMessageById(sMessageId+1);
        if(afterSMessage!=null&&(!afterSMessage.isHandled()))
        {
            synchronized(afterSMessage)
            {
                afterSMessage.notifyAll();
            }
        }
    }
    
    public void send(ITNMRequest request)
    {
        requestHistory.addRequest(request);
        JsonObject packageJson=new JsonObject();
        packageJson.addProperty(TNMKJS.CODE,request.getCode().toString());
        String encryptStrContent;
        request.setTime();
        ITNMResult result;
        switch(request.getCode())
        {
            case NONE:
                packageJson.add(TNMKJS.CONTENT,request.getContent());
                break;
            case DES:
                ITNMDES des=new TNMDES();
                result=des.encrypt(keyManager.getDESKey()
                                        ,request.getContent().toString());
                if(!result.isSuccess())
                {
                    ITNMParams params=new TNMParams();
                    params.setParam(TNMKJS.CODE,TNMCode.DES);
                    params.setParam(TNMKJS.REQUEST,request);
                    onError(14,params);
                }
                packageJson.addProperty(TNMKJS.CONTENT,(String)result.getResult());
                break;
            case RSA:
                ITNMRSA rsa=new TNMRSA();
                result=rsa.encrypt(request.getContent().toString());
                if(result.isSuccess())
                {
                    ITNMParams params=new TNMParams();
                    params.setParam(TNMKJS.CODE,TNMCode.RSA);
                    params.setParam(TNMKJS.REQUEST,request);
                    onError(14,params);
                }
                packageJson.addProperty(TNMKJS.CONTENT,(String)result.getResult());
                break;
        }
        if(TNMCommunicateConfig.getDefault().SOCKET_DEBUG_ENABLE)
        {
            System.out.println("Send:"+packageJson.toString());
        }
        putMessageToSocket(packageJson.toString());
    }
    
    abstract protected void putMessageToSocket(String str);
    
    public ITNMKeyManager getKeyManager()
    {
        return keyManager;
    }
    
    private boolean isRequest(String cmd)
    {
        String suffixCmd=cmd.substring(cmd.length()-TNMKJS.LAST_REQUEST.length(),cmd.length());
        if(suffixCmd.equals(TNMKJS.LAST_REQUEST))
            return true;
        else
            return false;
    }
    
    private boolean isNotify(String cmd)
    {
        String suffixCmd=cmd.substring(cmd.length()-TNMKJS.LAST_NOTIFY.length(),cmd.length());
        if(suffixCmd.equals(TNMKJS.LAST_NOTIFY))
            return true;
        else
            return false;
    }
    
    private boolean isResponse(String cmd)
    {
        String suffixCmd=cmd.substring(cmd.length()-TNMKJS.LAST_RESPONSE.length(),cmd.length());
        if(suffixCmd.equals(TNMKJS.LAST_RESPONSE))
            return true;
        else
            return false;
    }

    protected void onError(int code, ITNMParams params) 
    {
        for(int i=0;i<eventHandlers.size();i++)
        {
            ITNMConnectionEventHandler eventHandler=eventHandlers.get(i);
            eventHandler.onError(this,code,params);
        }
    }

    @Override
    public void handshaked()
    {
        this.bHandshaked=true;
        onHandshaked();
    }

    @Override
    public boolean isHandshaked()
    {
        return this.bHandshaked;
    }

    @Override
    public void setDesKey(String desKey)
    {
        keyManager.setDESKey(desKey);
    }
    
    protected void onDisconnected()
    {
        for(int i=0;i<eventHandlers.size();i++)
        {
            ITNMConnectionEventHandler eventHandler=eventHandlers.get(i);
            eventHandler.onDisconnectd(this);
        }
    }
    
    protected void onHandshaked()
    {
        for(int i=0;i<eventHandlers.size();i++)
        {
            ITNMConnectionEventHandler eventHandler=eventHandlers.get(i);
            eventHandler.onHandshaked(this);
        }
    }
    
    @Override
    public void addEventHandler(ITNMConnectionEventHandler eventHandler) 
    {
        eventHandlers.add(eventHandler);
    }

    @Override
    public void removeEventHandler(ITNMConnectionEventHandler eventHandler) 
    {
        eventHandlers.remove(eventHandler);
    }

    @Override
    public void removeEventHandler(int index) 
    {
        eventHandlers.remove(index);
    }

    @Override
    public void clearEventHandler() 
    {
        eventHandlers.clear();
    }

    @Override
    public void setProperty(String key, Object value) 
    {
        propeties.setParam(key,value);
    }

    @Override
    public Object getProperty(String key)
    {
        return propeties.getParam(key);
    }

    @Override
    public void clearProperty()
    {
        propeties.clear();
    }
}
