package tnm.communicate;

public interface ITNMKeyManager 
{
    public void setDESKey(String desKey);
    
    public String getDESKey();
}
