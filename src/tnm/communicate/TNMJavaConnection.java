package tnm.communicate;

import tnm.javalib.TNMParams;
import tnm.javalib.ITNMParams;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import tnm.javalib.TNMKJS;

public class TNMJavaConnection extends TNMConnection implements ITNMConnection
{
    protected Socket socket;
    protected DataInputStream dataInputStream;
    protected DataOutputStream dataOutputStream;
    protected Thread listenThread;
            
    public TNMJavaConnection(String host,int port)
    {   
        try 
        {
            this.socket=new Socket(host,port);
            this.socket.setKeepAlive(TNMCommunicateConfig.CONNECTION_KEEP_ALIVE);
            this.socket.setSoTimeout(TNMCommunicateConfig.SOCKET_SO_TIME_OUT_MILLIS);
            dataInputStream=new DataInputStream(socket.getInputStream());
            dataOutputStream=new DataOutputStream(socket.getOutputStream());
            run();
        } 
        catch (Exception ex) 
        {
            ITNMParams params=new TNMParams();
            params.setParam(TNMKJS.EXCEPTION,ex);
            params.setParam(TNMKJS.CONNECTION,this);
            onError(1,params);
        }
    }
    
    public void run()
    {   
        listenThread=new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    while(true)
                    {
                        final String dataStr=readMessage();
                        Thread handlingThread=new Thread()
                        {
                            @Override
                            public void run()
                            {
                                onMessageFromSocket(dataStr);
                            }
                        };
                        handlingThread.setPriority(TNMCommunicateConfig.HANDLING_THREAD_PRIORITY);
                        handlingThread.start();
                    }
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    ITNMParams params=new TNMParams();
                    params.setParam(TNMKJS.EXCEPTION,ex);
                    onError(2,params);
                }
            }
        };
        listenThread.setPriority(TNMCommunicateConfig.DATA_LISTEN_THREAD_PRIORITY);
        listenThread.start();
    }
    
    protected String readMessage() throws IOException
    {
        String dataStr=dataInputStream.readUTF();
        return dataStr;
    }
    
    protected void putMessageToSocket(String str)
    {
        try
        {
            writeMessage(str);
        } 
        catch (Exception ex)
        {
            ex.printStackTrace();
            ITNMParams params=new TNMParams();
            params.setParam(TNMKJS.EXCEPTION,ex);
            onError(4,params);
        }
    }
    
    protected void writeMessage(String str) throws IOException
    {
        dataOutputStream.writeUTF(str);
    }
    
    @Override
    public String getAddress() 
    {
        return socket.getLocalAddress().getHostAddress();
    }

    @Override
    public void disconect() 
    {
        try 
        {
            listenThread.stop();
            this.socket.close();
            if(dataInputStream!=null)
                dataInputStream.close();
            if(dataOutputStream!=null)
                dataOutputStream.close();
            onDisconnected();
        } 
        catch (IOException ex) 
        {
            ITNMParams params=new TNMParams();
            params.setParam(TNMKJS.EXCEPTION,ex);
            onError(60,params);
        }
    }
}
