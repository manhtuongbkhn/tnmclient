package tnm.communicate;

import tnm.javalib.ITNMObject;

public interface ITNMSMessageListener extends ITNMObject
{
    public int getWaitResponseTimeMillis();
    
    public void setWaitResponseTimeMillis(int timeMillis);
}
