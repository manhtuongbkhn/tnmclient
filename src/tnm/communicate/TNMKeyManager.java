package tnm.communicate;

import tnm.javalib.TNMObject;

public class TNMKeyManager extends TNMObject implements ITNMKeyManager
{
    private String desKey;
    
    @Override
    public void setDESKey(String desKey)
    {
        this.desKey=desKey;
    }
    
    @Override
    public String getDESKey()
    {
        return desKey;
    }
}
