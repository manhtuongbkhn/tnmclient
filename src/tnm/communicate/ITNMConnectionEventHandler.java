package tnm.communicate;

import tnm.javalib.ITNMObject;
import tnm.javalib.ITNMParams;
import tnm.message.ITNMSMessage;

public interface ITNMConnectionEventHandler extends ITNMObject
{
    public void onHandshaked(ITNMConnection connection);
    
    public void onDisconnectd(ITNMConnection connection);
    
    public void onError(ITNMConnection connection,int code,ITNMParams params);
    
    public void onSMessage(ITNMConnection connection,ITNMSMessage message);
}
