package tnm.communicate;

import tnm.message.ITNMRequest;
import tnm.javalib.ITNMObject;

public interface ITNMConnection extends ITNMObject
{
    public void send(ITNMRequest request);
    
    public String getAddress();
    
    public void handshaked();
    
    public boolean isHandshaked();
    
    public void setDesKey(String desKey);
    
    public void disconect();
    
    public ITNMKeyManager getKeyManager();
    
    public void addEventHandler(ITNMConnectionEventHandler eventHandler);
    
    public void removeEventHandler(ITNMConnectionEventHandler eventHandler);
    
    public void removeEventHandler(int index);
    
    public void clearEventHandler();
    
    public void setProperty(String key, Object value);
    
    public Object getProperty(String key);

    public void clearProperty(); 
}
