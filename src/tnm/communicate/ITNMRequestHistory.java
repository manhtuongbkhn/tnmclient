package tnm.communicate;

import tnm.javalib.ITNMObject;
import tnm.message.ITNMRequest;

public interface ITNMRequestHistory extends ITNMObject
{
    public void addRequest(ITNMRequest request);
}
