package tnm.communicate;

import tnm.javalib.ITNMObject;
import tnm.javalib.ITNMResult;
import tnm.message.ITNMSMessage;

public interface ITNMSMessageHistory extends ITNMObject
{
    public ITNMResult addSMessage(ITNMSMessage sMessage);
    
    public ITNMSMessage getSMessageById(int id);
}
