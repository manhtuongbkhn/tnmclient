package tnm.communicate;

import tnm.javalib.TNMObject;
import tnm.javalib.TNMArrayList;
import tnm.message.ITNMRequest;

public class TNMRequestHistory extends TNMObject implements ITNMRequestHistory
{
    private TNMArrayList<ITNMRequest> requests;
    
    public TNMRequestHistory()
    {
        requests=new TNMArrayList<ITNMRequest>();
    }
    
    synchronized public void addRequest(ITNMRequest request)
    {
        request.setId(getNewId());
        if(requests.size()>=TNMCommunicateConfig.REQUEST_HISTORY_COUNT)
            requests.remove(0);
        requests.add(request);
    }
    
    synchronized private int getNewId()
    {
        int size=requests.size();
        if(size>0)
        {
            ITNMRequest request=requests.get(size-1);
            return request.getId()+1;
        }
        else
            return 1;
    }
}
