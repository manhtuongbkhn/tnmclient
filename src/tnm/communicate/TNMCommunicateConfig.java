package tnm.communicate;

import tnm.javalib.ITNMParams;
import tnm.javalib.TNMObject;
import tnm.javalib.TNMParams;

public class TNMCommunicateConfig extends TNMObject
{  
    private static ITNMParams<TNMCommunicateConfig> configs=new TNMParams<TNMCommunicateConfig>();
    
    public static void setDefault(TNMCommunicateConfig config)
    {
        setInstance(null,config);
    }
    
    public static void setInstance(String name,TNMCommunicateConfig config)
    {
        if(name==null)
            name="default";
        if(configs.getParam(name)==null)
            configs.setParam(name,config);
    }
    
    public static TNMCommunicateConfig getDefault()
    {
        return getInstance(null);
    }
    
    public static TNMCommunicateConfig getInstance(String name)
    {
        if(name==null)
            name="default";
        TNMCommunicateConfig config=configs.getParam(name);
        if(config==null)
        {
            config=new TNMCommunicateConfig();
            configs.setParam(name,config);
        }
        return config;
    }
    
    public boolean SOCKET_DEBUG_ENABLE=true;      
    
    public TNMCommunicateConfig()
    {
        SOCKET_DEBUG_ENABLE=true;
    }
    
    public static final boolean CONNECTION_KEEP_ALIVE=true;
    public static final int RESPONSE_HISTORY_COUNT=20;
    public static final int REQUEST_HISTORY_COUNT=10;
    public static final int CONNECTION_TIME_OUT_MILLIS=400;
    public static final int COMMUNICATOR_TIME_OUT_MILLIS=0;
    public static final int PERMISSION_RESPONSE_DELAY_TIME_MILLIS=30000;
    public static final int PERMISSION_DELTA_RESPONSE_ID=10;
    public static final int PERMISSION_DELTA_RESPONSE_MILLIS=0;
    public static final int PERMISSION_AVERAGE_RESPONSE_IN_SECOND=10;
    public static final int SOCKET_SO_TIME_OUT_MILLIS=3600000;
    public static final int DELAY_SMESSAGE_TIME_MILLIS=1000;
    
    public final static int CONNECTION_LISTEN_THREAD_PRIORITY=5;
    public final static int DATA_LISTEN_THREAD_PRIORITY=1;
    public final static int HANDLING_THREAD_PRIORITY=1;
    public final static int WAIT_RESPONSE_THREAD_PRIORITY=1;
}
