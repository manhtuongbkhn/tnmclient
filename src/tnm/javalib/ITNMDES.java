package tnm.javalib;

public interface ITNMDES extends ITNMObject
{
    public ITNMResult encrypt(String StrEncode,String message);

    public ITNMResult decrypt(String StrEncode,String encrytMessage);
    
    public ITNMResult createKey();
    
    public boolean checkKey(String desKey);
}
