package tnm.javalib;

import java.io.Serializable;
import java.util.Map;

public interface ITNMHashMap<K,V> extends Map<K,V>, Cloneable, Serializable
{
    public K removeValue(V value);
    
    public K getKey(V value);
    
    public Object clone();
}
