package tnm.javalib;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import java.util.Date;
import java.util.List;

public class TNMFunction 
{   
    public static JsonArray convertBytesToJsonArray(byte [] bytes)
    {
        JsonArray jsonArray=new JsonArray();
        for(int i=0;i<bytes.length;i++)
        {
            jsonArray.add(new JsonPrimitive(bytes[i]));
        }
        return jsonArray;
    }
    
    public static byte[] convertJsonArrayToBytes(JsonArray jsonArray)
    {
        byte[] bytes=new byte[jsonArray.size()];
        for(int i=0;i<jsonArray.size();i++)
        {
            bytes[i]=jsonArray.get(i).getAsByte();
        }
        return bytes;
    }
    
    public static byte[] convertStringCodeToByteArrayCode(String strCode)
    {
        Gson gson=new Gson();
        JsonArray jsonArrayCode=gson.fromJson(strCode,JsonArray.class);
        return convertJsonArrayToBytes(jsonArrayCode);
    }
    
    public static String convertByteArrayCodeToStringCode(byte[] byteArrayCode)
    {
        JsonArray jsonArrayCode=convertBytesToJsonArray(byteArrayCode);
        return jsonArrayCode.toString();
    }
    
    public static ITNMArrayList<String> convertJsonArrayToStringList(JsonArray jsonArray)
    {
        ITNMArrayList<String> list=new TNMArrayList<String>();
        for(int i=0;i<jsonArray.size();i++)
        {
            String str=jsonArray.get(i).getAsString();
            list.add(str);
        }
        return list;
    }
    
    public static JsonArray convertStringListToJsonArray(ITNMArrayList<String> list)
    {
        JsonArray jsonArray=new JsonArray();
        for(int i=0;i<list.size();i++)
        {
            String str=list.get(i);
            jsonArray.add(new JsonPrimitive(str));
        }
        return jsonArray;
    }
    
    public static ITNMArrayList<String> convertPathStrToPathList(String pathStr)
    {
        ITNMArrayList<String> pathList=new TNMArrayList<String>();
        String[] tmpArr=pathStr.split("/");
        for(int i=0;i<tmpArr.length;i++)
        {
            String path=tmpArr[i];
            pathList.add(path);
        }
        return pathList;
    }
    
    public static String convertDateToString()
    {
        return convertDateToString(new Date());
    }
    
    public static String convertDateToString(Date date)
    {
        return date.getYear() + "-" + date.getMonth() + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    }
    
    public static Date convertStringToDate(String string)
    {
        return new Date();
    }
}
