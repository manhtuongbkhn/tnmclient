package tnm.javalib;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class TNMHashMap<K,V> extends HashMap<K,V> implements ITNMHashMap<K,V>
{
    public K removeValue(V value)
    {
        Iterator<Entry<K,V>> iterator=entrySet().iterator();
        while (iterator.hasNext()) 
        {            
            Entry<K,V> entry=iterator.next();
            if(entry.getValue()==value)
            {
                remove(entry.getKey());
                return entry.getKey();
            }
        }
        return null;
    }

    @Override
    public K getKey(V value) 
    {
        Iterator<Entry<K,V>> iterator=entrySet().iterator();
        while (iterator.hasNext()) 
        {            
            Entry<K,V> entry=iterator.next();
            if(entry.getValue()==value)
            {
                return entry.getKey();
            }
        }
        return null;
    }
}
