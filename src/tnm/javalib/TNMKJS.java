package tnm.javalib;

public class TNMKJS 
{
    //Communicate
    public final static String USED_CODE="used_code";
    public final static String EXTENSION="extension";
    public final static String SCOPE="scope";
    public final static String HANDLER_PATH="handler_path";
    public final static String CMD="cmd";
    public static final String LAST_RESPONSE="_response";
    public static final String LAST_REQUEST="_request";
    public static final String LAST_NOTIFY="_notify";
    public final static String ADDRESS="address";
    
    //
    public final static String REQUEST="request";
    public final static String RESPONSE="response";
    public final static String CODE="code";
    public final static String EXCEPTION="exception";
    public final static String THROWABLE="throwable";
    public final static String DEFAULT="default";
    public final static String CONTENT="content";
    public final static String ID="id";
    public final static String VALUE="value";
    public final static String DATA="data";
    public final static String EVENT_TYPE="event_type";
    public final static String PATH="path";
    public final static String NODE_ID="node_id";
    public final static String NODE_TYPE="node_type";
    public final static String ERROR="error";
    
    public final static String TIME="time";
    
    //Entities
    public final static String SOCKET="socket";
    public final static String CONNECTION="connection";
//    public final static String COMMUNICATOR="communicator";
    public final static String USER_ID="user_id";
    public final static String USER="user";
    public final static String ADMIN="admin";
    public final static String ZONE="zone";
    public final static String ZONE_ID="zone_id";
    public final static String ZONE_NAME="zone_name";
    public final static String ROOM_GROUP="room_group";
    public final static String ROOM="room";
    public final static String ROOM_ID="room_id";
    public final static String ROOM_NAME="room_name";
    
    public final static String HANDLER="handler";
    public final static String TOKEN="token";
    public final static String TYPE="type";
    public final static String POSITION="position";
    
    public final static String REQUEST_ID="request_id";
    public final static String RESPONSE_ID="response_id";
    public final static String NOTIFY_ID="notify_id"; 
    
    public final static String SUCCESS="success";
    public final static String MESSAGE="message";
    
    public final static String DES="des";
    public final static String RSA="rsa";
    
    public final static String BEFORE_VALUE="before_value";
    public final static String AFTER_VALUE="after_value";
    public final static String STATUS="status";    
    public final static String IS_RESPONSED="is_responsed";
}
