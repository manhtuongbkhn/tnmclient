package tnm.javalib;

import java.util.Iterator;

public class TNMParams<T> implements ITNMParams<T>
{
    private ITNMHashMap<String,T> map;
    
    public TNMParams()
    {
        map=new TNMHashMap<String,T>();
    }
    
    public TNMParams(TNMHashMap<String,T> map)
    {
        this.map=map;
    }
    
    public int addParam(T value)
    {
        Integer size=map.size();
        map.put(size.toString(),value);
        return size;
    }
    
    public boolean setParam(String key,T value)
    {
        map.put(key,value);
        return true;
    }
    
    public T removePamram(int index)
    {
        return (T)this.removeParam(new Integer(index).toString());
    }
    
    public T removeParam(String key)
    {
        return (T) map.remove(key);
    }
    
    public String removeParam(T t)
    {
        return map.removeValue(t);
    }
    
    public T getParam(Integer index)
    {
        return (T) map.get(index.toString());
    }
    
    public T getParam(String key)
    {
        return (T) map.get(key);
    }
    
    public String getKey(T t)
    {
        return map.getKey(t);
    }
    
    public void clear()
    {
        map.clear();
    }
    
    public int getParamCount()
    {
        return map.size();
    }
    
    @Override
    public boolean containsKey(String key) 
    {
        return map.containsKey(key);
    }
    
    public Iterator<String> getKeys()
    {
        return map.keySet().iterator();
    }
    
    public String showContent()
    {
        String showStr=new String();
        Iterator<String> keys=getKeys();
        showStr=showStr.concat("\nkey : value\n");
        while(keys.hasNext())
        {
            String key=keys.next();
            showStr=showStr.concat(key+" : ");
            Object value=getParam(key);
            showStr=showStr.concat(value.getClass().getName()+"\n");
        }
        return showStr;
    }

    @Override
    public ITNMParams<T> clone() 
    {
        TNMHashMap<String,T> cloneMap=(TNMHashMap<String,T>)map.clone();
        ITNMParams<T> cloneParams=new TNMParams<T>(cloneMap);
        return cloneParams;
    }   
}
