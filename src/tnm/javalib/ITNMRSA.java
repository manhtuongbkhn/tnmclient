package tnm.javalib;

public interface ITNMRSA 
{
    public ITNMResult init(String strPublicKey,String strPrivateKey);
    
    public ITNMResult encrypt(String message);
    
    public ITNMResult decrypt(String codeMessage);
}
