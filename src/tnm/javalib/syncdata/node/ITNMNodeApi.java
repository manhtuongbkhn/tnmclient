package tnm.javalib.syncdata.node;

import com.google.gson.JsonObject;
import tnm.javalib.ITNMObject;
import tnm.javalib.ITNMParams;

public interface ITNMNodeApi extends ITNMObject
{
    public boolean link(int index,ITNMArrayNode arrrayNode,ITNMElementNode childNode);
    
    public boolean link(int index,ITNMArrayNode arrrayNode,ITNMElementNode childNode,boolean fireEvent);
    
    public boolean link(int index,ITNMArrayNode arrrayNode,ITNMElementNode childNode,ITNMParams extendParams);
    
    public boolean link(int index,ITNMArrayNode arrrayNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams);
    
    public boolean link(ITNMArrayNode arrrayNode,ITNMElementNode childNode);
    
    public boolean link(ITNMArrayNode arrayNode,ITNMElementNode childNode,boolean fireEvent);
     
    public boolean link(ITNMArrayNode arrayNode,ITNMElementNode childNode,ITNMParams extendParams);
      
    public boolean link(ITNMArrayNode arrrayNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams);
    
    public boolean link(String field,ITNMObjectNode objectNode,ITNMElementNode childNode);
    
    public boolean link(String field,ITNMObjectNode objectNode,ITNMElementNode childNode,ITNMParams extendParams);
    
    public boolean link(String field,ITNMObjectNode objectNode,ITNMElementNode childNode,boolean fireEvent);
    
    public boolean link(String field,ITNMObjectNode objectNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams);
    
    public boolean unlink(String field,ITNMObjectNode objectNode);
    
    public boolean unlink(String field,ITNMObjectNode objectNode,boolean fireEvent);
    
    public boolean unlink(String field,ITNMObjectNode objectNode,ITNMParams extendParams);
    
    public boolean unlink(String field,ITNMObjectNode objectNode,boolean fireEvent,ITNMParams extendParams);
    
    public boolean unlink(ITNMObjectNode objectNode,ITNMElementNode childNode);
    
    public boolean unlink(ITNMObjectNode objectNode,ITNMElementNode childNode,boolean fireEvent);
    
    public boolean unlink(ITNMObjectNode objectNode,ITNMElementNode childNode,ITNMParams extendParams);
    
    public boolean unlink(ITNMObjectNode objectNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams);
    
    public void unlinkAll(ITNMObjectNode objectNode);
        
    public void unlinkAll(ITNMObjectNode objectNode,boolean fireEvent);
            
    public void unlinkAll(ITNMObjectNode objectNode,ITNMParams extendParams);
    
    public void unlinkAll(ITNMObjectNode objectNode,boolean fireEvent,ITNMParams extendParams);
    
    public boolean unlink(int index,ITNMArrayNode arrayNode);
    
    public boolean unlink(int index,ITNMArrayNode arrayNode,boolean fireEvent);
    
    public boolean unlink(int index,ITNMArrayNode arrayNode,ITNMParams extendParams);
    
    public boolean unlink(int index,ITNMArrayNode arrayNode,boolean fireEvent,ITNMParams extendParams);
    
    public boolean unlink(ITNMArrayNode arrayNode,ITNMElementNode childNode);
    
    public boolean unlink(ITNMArrayNode arrayNode,ITNMElementNode childNode,boolean fireEvent);
    
    public boolean unlink(ITNMArrayNode arrayNode,ITNMElementNode childNode,ITNMParams extendParams);
    
    public boolean unlink(ITNMArrayNode arrayNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams);
    
    public void unlinkAll(ITNMArrayNode arrayNode);
    
    public void unlinkAll(ITNMArrayNode arrayNode,boolean fireEvent);
        
    public void unlinkAll(ITNMArrayNode arrayNode,ITNMParams extendParams);
    
    public void unlinkAll(ITNMArrayNode arrayNode,boolean fireEvent,ITNMParams extendParams);
    
    public ITNMElementNode getNode(long id);
    
    public boolean addNode(ITNMElementNode elementNode);
    
    public ITNMElementNode removeNode(long id);
    
    public ITNMElementNode removeNode(String type,long id);
    
    public long removeNode(ITNMElementNode elementNode);
    
    public void onNewNotify(JsonObject data);
}
