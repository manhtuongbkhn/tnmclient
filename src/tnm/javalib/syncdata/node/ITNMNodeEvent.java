package tnm.javalib.syncdata.node;

import com.google.gson.JsonObject;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMObject;
import tnm.javalib.ITNMParams;

public interface ITNMNodeEvent extends ITNMObject
{
    public TNMEventType getEventType();
    
    public ITNMElementNode getNode();
    
    public ITNMArrayList<String> getPaths();
    
    public ITNMParams getExtendParams();
    
    public int getId();
    
    public Object clone();
    
    public JsonObject toJson();
}
