package tnm.javalib.syncdata.node;

import tnm.javalib.syncdata.TNMSyncConfig;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMHashMap;
import tnm.javalib.ITNMParams;
import tnm.javalib.TNMArrayList;
import tnm.javalib.TNMHashMap;

abstract public class TNMParentNode extends TNMElementNode implements ITNMParentNode
{
    protected ITNMHashMap<Integer,ITNMArrayList<Integer>> handledEventIdsMap;
    
    public TNMParentNode(String type,long id,String token)
    {
        super(type,id,token);
        handledEventIdsMap=new TNMHashMap<Integer,ITNMArrayList<Integer>>();
    }
    
    abstract protected String getKey(ITNMElementNode node);
    
    @Override
    public void onListenNodeChange(ITNMElementNode elementNode, ITNMNodeEvent event) 
    {
        int nodeHashCode=event.getNode().hashCode();
        ITNMArrayList<Integer> handledEventIds=handledEventIdsMap.get(nodeHashCode);
        if(handledEventIds==null)
        {
            handledEventIds=new TNMArrayList<Integer>();
            handledEventIdsMap.put(new Integer(nodeHashCode),handledEventIds);
        }
        boolean handled=false;
        for(int i=0;i<handledEventIds.size();i++)
        {
            int eventId=handledEventIds.get(i);
            if(event.getId()==eventId)
            {
                handled=true;
                break;
            }
        }
        
        if(!handled)
        {
            handledEventIds.add(event.getId());
            if(handledEventIds.size()>TNMSyncConfig.getDefault().EVENT_HISTORY_COUNT)
                handledEventIds.remove(0);
            String key=getKey(elementNode);
            event.getPaths().add(0,key);
            onChange(event);
        }
    }
    
    public void onListenNodeError(int code,String message,ITNMParams params)
    {
        
    }
    
    abstract public boolean isNull();
}
