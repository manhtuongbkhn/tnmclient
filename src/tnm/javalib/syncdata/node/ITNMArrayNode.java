package tnm.javalib.syncdata.node;

import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMParams;

public interface ITNMArrayNode extends ITNMParentNode
{   
    public ITNMElementNode getChild(int index);
    
    public int addChild(ITNMElementNode elementNode);
    
    public int addChild(ITNMElementNode elementNode,boolean fireEvent);
    
    public int addChild(ITNMElementNode elementNode,ITNMParams extendParams);
    
    public int addChild(ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams);
    
    public int addChild(int index,ITNMElementNode elementNode);
    
    public int addChild(int index,ITNMElementNode elementNode,boolean fireEvent);
    
    public int addChild(int index,ITNMElementNode elementNode,ITNMParams extendParams);
    
    public int addChild(int index,ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams);
    
    public ITNMElementNode removeChild(int index);
    
    public ITNMElementNode removeChild(int index,boolean fireEvent);
    
    public ITNMElementNode removeChild(int index,ITNMParams extendParams);
    
    public ITNMElementNode removeChild(int index,boolean fireEvent,ITNMParams extendParams);
    
    public boolean removeChild(ITNMElementNode elementNode);
    
    public boolean removeChild(ITNMElementNode elementNode,boolean fireEvent);
    
    public boolean removeChild(ITNMElementNode elementNode,ITNMParams extendParams);
    
    public boolean removeChild(ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams);
    
    public ITNMArrayList<ITNMElementNode> getChildNodes();
}
