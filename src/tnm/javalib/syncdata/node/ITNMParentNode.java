package tnm.javalib.syncdata.node;

public interface ITNMParentNode extends ITNMElementNode, ITNMNodeEventListener
{    
    public int getChildNodeCount();
}
