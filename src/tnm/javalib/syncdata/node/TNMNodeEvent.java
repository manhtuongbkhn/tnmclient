package tnm.javalib.syncdata.node;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMParams;
import tnm.javalib.TNMArrayList;
import tnm.javalib.TNMFunction;
import tnm.javalib.TNMKJS;
import tnm.javalib.TNMObject;
import tnm.javalib.TNMParams;

public class TNMNodeEvent extends TNMObject implements ITNMNodeEvent
{
    protected TNMEventType eventType;
    protected ITNMElementNode node;
    protected ITNMArrayList<String> paths;
    protected ITNMParams extendParams;
    protected int id;
    
    public TNMNodeEvent(TNMEventType eventType,ITNMElementNode node,int eventId)
    {
        this(eventType,node,new TNMArrayList<String>(),new TNMParams<>(),eventId);
    }
    
    public TNMNodeEvent(TNMEventType eventType,ITNMElementNode node,ITNMArrayList<String> paths,int eventId)
    {
        this(eventType,node,paths,new TNMParams<>(),eventId);
    }
    
    public TNMNodeEvent(TNMEventType eventType,ITNMElementNode node,ITNMParams<Object> params,int eventId)
    {
        this(eventType,node,new TNMArrayList<String>(),params,eventId);
    }
    
    public TNMNodeEvent(TNMEventType eventType,ITNMElementNode node
                        ,ITNMArrayList<String> paths,ITNMParams<Object> extendParams,int eventId)
    {
        this.eventType=eventType;
        this.node=node;
        this.paths=paths;
        this.extendParams=extendParams;
        this.id=eventId;
    }
    
    public TNMEventType getEventType()
    {
        return eventType;
    }
    
    public ITNMElementNode getNode()
    {
        return node;
    }
    
    public ITNMArrayList<String> getPaths()
    {
        return paths;
    }
    
    public ITNMParams getExtendParams()
    {
        return extendParams;
    }
    
    public int getId()
    {
        return id;
    }
    
    public Object clone()
    {
        ITNMArrayList<String> clonePaths=(ITNMArrayList<String>) this.paths.clone();
        ITNMParams cloneParams=(ITNMParams) extendParams.clone();
        return new TNMNodeEvent(eventType,node,clonePaths,cloneParams,id);
    }
    
    public JsonObject toJson()
    {
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty(TNMKJS.EVENT_TYPE,eventType.toString());
        jsonObject.add(TNMKJS.VALUE,node.toJson());
        JsonArray jsonArray=TNMFunction.convertStringListToJsonArray(paths);
        jsonObject.add(TNMKJS.PATH,jsonArray);
        return jsonObject;
    }
}
