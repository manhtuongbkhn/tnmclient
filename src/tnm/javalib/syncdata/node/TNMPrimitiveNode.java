package tnm.javalib.syncdata.node;  

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import java.util.Date;
import tnm.javalib.ITNMParams;
import tnm.javalib.TNMFunction;
import tnm.javalib.TNMParams;

public class TNMPrimitiveNode extends TNMElementNode implements ITNMPrimitiveNode
{
    private JsonPrimitive data;
    
    public TNMPrimitiveNode(String type,long id,String token)
    {
        this(type,id,token,null);
    }
    
    public TNMPrimitiveNode(JsonPrimitive jsonPrimitive)
    {
        this(null,0,null,jsonPrimitive);
    }
    
    public TNMPrimitiveNode(Number number)
    {
        this(null,0,null,new JsonPrimitive(number));
    }
    
    public TNMPrimitiveNode(String str)
    {
        this(null,0,null,new JsonPrimitive(str));
    }
    
    public TNMPrimitiveNode(Date date)
    {
        this(null,0,null,new JsonPrimitive(TNMFunction.convertDateToString(date)));
    }
    
    public TNMPrimitiveNode(Boolean boo)
    {
        this(null,0,null,new JsonPrimitive(boo));
    }
    
    protected TNMPrimitiveNode(String type,long id,String token,JsonPrimitive jsonPrimitive)
    {
        super(type,id,token);
        if(jsonPrimitive!=null)
            setContent(jsonPrimitive);
    }
    
    public int getInteger()
    {
        return data.getAsInt();
    }
    
    public float getFloat()
    {
        return data.getAsFloat();
    }
    
    public String getString()
    {
        return data.getAsString();
    }
    
    public Date getDate()
    {
        return TNMFunction.convertStringToDate(data.getAsString());
    }
    
    public boolean getBoolean()
    {
        return data.getAsBoolean();
    }
    
    @Override
    public JsonElement toJson()
    {
        return data;
    }
    
    private void setContent(JsonPrimitive jsonPrimitive)
    {
        this.data=jsonPrimitive;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public boolean isNull()
    {
        if(data==null)
            return true;
        else
            return false;
    }
    
    @Override
    public void replace(JsonElement jsonElement) 
    {
        replace(jsonElement,true,new TNMParams());
    }
    
    public void replace(JsonElement jsonElement,ITNMParams extendParams)
    {
        replace(jsonElement,true,extendParams);
    }
    
    public void replace(JsonElement jsonElement,boolean fireEvent) 
    {
        replace(jsonElement,fireEvent,new TNMParams());
    }
    
    @Override
    public void replace(JsonElement jsonElement,boolean fireEvent,ITNMParams extendParams) 
    {
        if(jsonElement==null || (jsonElement instanceof JsonNull))
        {
            data=null;
            if(fireEvent)
            {
                ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.UPDATED,this,extendParams,initMaxEventId());
                onChange(event);
            }
            return ;
        }       
        
        if(jsonElement instanceof JsonPrimitive)
        {
            setContent((JsonPrimitive) jsonElement);
            if(fireEvent)
            {
                ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.UPDATED,this,extendParams,initMaxEventId());
                onChange(event);
            }
        }
        else
            throw new UnsupportedOperationException("Input must Number,String,Boolean,Date,JsonPrimitive");
    }
}
