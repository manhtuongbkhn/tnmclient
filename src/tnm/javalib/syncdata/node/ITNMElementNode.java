package tnm.javalib.syncdata.node;

import com.google.gson.JsonElement;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMObject;
import tnm.javalib.ITNMParams;

public interface ITNMElementNode extends ITNMObject
{       
    public void replace(JsonElement jsonElement);
    
    public void replace(JsonElement jsonElement,boolean fireEvent);
    
    public void replace(JsonElement jsonElement,ITNMParams extendParams);
    
    public void replace(JsonElement jsonElement,boolean fireEvent,ITNMParams extendParams);
    
    public void addNodeEventListenser(ITNMNodeEventListener eventListener);
    
    public void removeNodeEventListenser(ITNMNodeEventListener eventListener);
    
    public void removeNodeEventListenser(int index);
    
    public boolean isNull();
    
    public void setId(long id);
    
    public long getId();
    
    public String getType();
    
    public void setType(String type);
    
    public JsonElement toJson();
    
    public ITNMElementNode getLineage(ITNMArrayList<String> paths);
    
    public ITNMElementNode getLineage(String paths);
    
    public void setToken(String token);
    
    public String getToken();
    
    public void error(int code,String message,ITNMParams params);
}
