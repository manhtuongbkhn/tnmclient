package tnm.javalib.syncdata.node;

import tnm.javalib.TNMFunction;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMParams;
import tnm.javalib.TNMArrayList;
import tnm.javalib.TNMKJS;
import tnm.javalib.TNMObject;

abstract public class TNMElementNode extends TNMObject implements ITNMElementNode
{
    protected String type;
    protected long id;
    protected String token;
    protected ITNMArrayList<ITNMNodeEventListener> eventListeners;
    protected int maxEventId=1;
    
    public TNMElementNode()
    {
        this(null,0,null);
    }
    
    public TNMElementNode(long id)
    {
        this(null,id,null);
    }
    
    public TNMElementNode(long id,String token)
    {
        this(null,id,token);
    }   
    
    public TNMElementNode(String type,long id,String token)
    {
        super();
        this.type=type;
        this.id=id;
        this.token=token;
        eventListeners=new TNMArrayList<ITNMNodeEventListener>();
    }
    
    @Override
    public void addNodeEventListenser(ITNMNodeEventListener eventListener)
    {
        eventListeners.add(eventListener);
    }
    
    @Override
    public void removeNodeEventListenser(ITNMNodeEventListener eventListener)
    {
        eventListeners.remove(eventListener);
    }
    
    @Override
    public void removeNodeEventListenser(int index)
    {
        eventListeners.remove(index);
    }
    
    @Override
    public void setId(long id)
    {
        this.id=id;
    }
    
    @Override
    public long getId()
    {
        return id;
    }
    
    @Override
    public String getType()
    {
        if(type==null)
            return TNMKJS.DEFAULT;
        else
            return type;
    }
    
    @Override
    public void setType(String type)
    {
        this.type=type;
    }
    
    protected void onChange(ITNMNodeEvent event)
    {
        fireEvent(event);
    }
    
    protected void fireEvent(ITNMNodeEvent event)
    {
        for(int i=0;i<eventListeners.size();i++)
        {
            ITNMNodeEvent cloneEvent=(ITNMNodeEvent) event.clone();
            ITNMNodeEventListener eventListener=eventListeners.get(i);
            eventListener.onListenNodeChange(this,cloneEvent);
        }
    }
    
    @Override
    public ITNMElementNode getLineage(ITNMArrayList<String> paths)
    {
        if(paths.size()==0)
            return this;
        if(this instanceof ITNMPrimitiveNode)
            return null;
        if(this instanceof ITNMParentNode)
        {
            ITNMElementNode elementNode=null;
            String field=paths.get(0);
            if(this instanceof ITNMObjectNode)
            {
                ITNMObjectNode objectNode=(ITNMObjectNode) this;
                elementNode=objectNode.getChild(field);
            }
            
            if(this instanceof ITNMArrayNode)
            {
                ITNMArrayNode arrayNode=(ITNMArrayNode) this;
                Integer index=new Integer(field);
                elementNode=arrayNode.getChild(index);
            }
            paths.remove(0);
            return elementNode.getLineage(paths);
        }
        return null;
    }
    
    @Override
    public ITNMElementNode getLineage(String paths)
    {
        return getLineage(TNMFunction.convertPathStrToPathList(paths));
    }
    
    @Override
    public void setToken(String token)
    {
        this.token=token;
    }
    
    @Override
    public String getToken()
    {
        return token;
    }
    
    @Override
    public void error(int code,String message,ITNMParams params)
    {
        for(int i=0;i<eventListeners.size();i++)
        {
            ITNMNodeEventListener eventListener=eventListeners.get(i);
            eventListener.onListenNodeError(code,message,params);
        }
    }
    
    protected int initMaxEventId()
    {
        maxEventId++;
        return maxEventId;
    }
}
