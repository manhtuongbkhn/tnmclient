package tnm.javalib.syncdata.node;

import tnm.javalib.ITNMParams;

public interface ITNMNodeEventListener 
{
    public void onListenNodeChange(ITNMElementNode elementNode,ITNMNodeEvent event);
    
    public void onListenNodeError(int code,String message,ITNMParams params);
}
