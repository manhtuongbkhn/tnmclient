package tnm.javalib.syncdata.node;

import java.util.Date;

public interface ITNMPrimitiveNode  extends ITNMElementNode
{   
    public int getInteger();
    
    public float getFloat();
    
    public String getString();
    
    public Date getDate();
    
    public boolean getBoolean();
}
