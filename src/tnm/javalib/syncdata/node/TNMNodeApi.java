package tnm.javalib.syncdata.node;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.Iterator;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMHashMap;
import tnm.javalib.ITNMParams;
import tnm.javalib.TNMFunction;
import tnm.javalib.TNMHashMap;
import tnm.javalib.TNMKJS;
import tnm.javalib.TNMObject;
import tnm.javalib.TNMParams;

public class TNMNodeApi extends TNMObject implements ITNMNodeApi
{   
    private static ITNMParams<ITNMNodeApi> nodeApis=new TNMParams<ITNMNodeApi>();
    
    public static ITNMNodeApi getDefault()
    {
        return getInstance(null);
    }
    
    public static ITNMNodeApi getInstance(String name)
    {
        if(name==null)
            name="default";
        ITNMNodeApi nodeApi=nodeApis.getParam(name);
        if(nodeApi==null)
        {
            nodeApi=new TNMNodeApi();
            nodeApis.setParam(name,nodeApi);
        }
        return nodeApi;
    }
    
    private ITNMParams<TNMNodeManager> nodeManagers;
    
    protected TNMNodeApi()
    {
        nodeManagers=new TNMParams<TNMNodeManager>();
    }
    
    public boolean link(int index,ITNMArrayNode arrrayNode,ITNMElementNode childNode)
    {
         return link(index,arrrayNode,childNode,true,new TNMParams());
    }
    
    public boolean link(int index,ITNMArrayNode arrrayNode,ITNMElementNode childNode,boolean fireEvent)
    {
        return link(index,arrrayNode,childNode,fireEvent,new TNMParams());
    }
    
    public boolean link(int index,ITNMArrayNode arrrayNode,ITNMElementNode childNode,ITNMParams extendParams)
    {
        return link(index,arrrayNode,childNode,true,extendParams);
    }
    
    public boolean link(int index,ITNMArrayNode arrrayNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams)
    {
        arrrayNode.addChild(index,childNode,fireEvent,extendParams);
        childNode.removeNodeEventListenser(arrrayNode);
        return true;
    }
    
    public boolean link(ITNMArrayNode arrayNode,ITNMElementNode childNode)
    {
        return link(arrayNode,childNode,true,new TNMParams());
    }
    
    public boolean link(ITNMArrayNode arrayNode,ITNMElementNode childNode,boolean fireEvent)
    {
        return link(arrayNode,childNode,fireEvent,new TNMParams());
    }
    
    public boolean link(ITNMArrayNode arrayNode,ITNMElementNode childNode,ITNMParams extendParams)
    {
        return link(arrayNode,childNode,true,extendParams);
    }
    
    public boolean link(ITNMArrayNode arrayNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams)
    {
        int index=arrayNode.addChild(childNode,fireEvent,extendParams);
        childNode.addNodeEventListenser(arrayNode);
        return true;
    }
    
    public boolean link(String field,ITNMObjectNode objectNode,ITNMElementNode childNode)
    {
        return link(field,objectNode,childNode,true,new TNMParams());
    }
    
    public boolean link(String field,ITNMObjectNode objectNode,ITNMElementNode childNode,boolean fireEvent)
    {
        return link(field,objectNode,childNode,fireEvent,new TNMParams());
    }
    
    public boolean link(String field,ITNMObjectNode objectNode,ITNMElementNode childNode,ITNMParams extendParams)
    {
        return link(field,objectNode,childNode,true,extendParams);
    }
    
    public boolean link(String field,ITNMObjectNode objectNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams)
    {
        objectNode.addChild(field,childNode,fireEvent,extendParams);
        childNode.addNodeEventListenser(objectNode);
        return true;
    }
    
        public boolean unlink(String field,ITNMObjectNode objectNode)
    {
        return unlink(field,objectNode,true,new TNMParams());
    }
    
    public boolean unlink(String field,ITNMObjectNode objectNode,boolean  fireEvent)
    {
        return unlink(field, objectNode, fireEvent,new TNMParams());
    }
    
    public boolean unlink(String field,ITNMObjectNode objectNode,ITNMParams extendParams)
    {
        return unlink(field, objectNode,true,extendParams);
    }
    
    public boolean unlink(String field,ITNMObjectNode objectNode,boolean  fireEvent,ITNMParams extendParams)
    {
        ITNMElementNode childNode=objectNode.getChild(field);
        objectNode.removeChild(field,fireEvent,extendParams);
        if(childNode!=null)
            childNode.removeNodeEventListenser(objectNode);
        return true;
    }
    
    public boolean unlink(ITNMObjectNode objectNode,ITNMElementNode childNode)
    {
        return unlink(objectNode,childNode,true,new TNMParams());
    }
    
    public boolean unlink(ITNMObjectNode objectNode,ITNMElementNode childNode,boolean fireEvent)
    {
        return unlink(objectNode,childNode,fireEvent,new TNMParams());
    }
    
    public boolean unlink(ITNMObjectNode objectNode,ITNMElementNode childNode,ITNMParams extendParams)
    {
        return unlink(objectNode,childNode,true,extendParams);
    }
    
    public boolean unlink(ITNMObjectNode objectNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams)
    {
        objectNode.removeChild(childNode,fireEvent,extendParams);
        childNode.removeNodeEventListenser(objectNode);
        return true;
    }

    public void unlinkAll(ITNMObjectNode objectNode)
    {
        unlinkAll(objectNode,true,new TNMParams());
    }
        
    public void unlinkAll(ITNMObjectNode objectNode,boolean fireEvent)
    {
        unlinkAll(objectNode,fireEvent,new TNMParams());
    }
            
    public void unlinkAll(ITNMObjectNode objectNode,ITNMParams extendParams)
    {
        unlinkAll(objectNode,extendParams);
    }
    
    public void unlinkAll(ITNMObjectNode objectNode,boolean fireEvent,ITNMParams extendParams)
    {
        Iterator<String> fields=objectNode.getFields();
        if(fields.hasNext())
        {
            String field=fields.next();
            unlink(field,objectNode,fireEvent,extendParams);
        }
    }
    
    public boolean unlink(int index,ITNMArrayNode arrayNode)
    {
        return unlink(index, arrayNode, true,new TNMParams());
    }
    
    public boolean unlink(int index,ITNMArrayNode arrayNode,boolean fireEvent)
    {
        return unlink(index, arrayNode,fireEvent,new TNMParams());
    }
    
    public boolean unlink(int index,ITNMArrayNode arrayNode,ITNMParams extendParams)
    {
        return unlink(index, arrayNode,true,extendParams);
    }
    
    public boolean unlink(int index,ITNMArrayNode arrayNode,boolean fireEvent,ITNMParams extendParams)
    {
        ITNMElementNode elementNode=arrayNode.getChild(index);
        arrayNode.removeChild(index,fireEvent,extendParams);
        if(elementNode!=null)
            elementNode.removeNodeEventListenser(arrayNode);
        return true;
    }
    
    public boolean unlink(ITNMArrayNode arrayNode,ITNMElementNode childNode)
    {
        return unlink(arrayNode,childNode,true,new TNMParams());
    }
    
    public boolean unlink(ITNMArrayNode arrayNode,ITNMElementNode childNode,ITNMParams extendParams)
    {
        return unlink(arrayNode,childNode,true,extendParams);
    }
    
    public boolean unlink(ITNMArrayNode arrayNode,ITNMElementNode childNode,boolean fireEvents)
    {
        return unlink(arrayNode, childNode, fireEvents, new TNMParams());
    }
    
    public boolean unlink(ITNMArrayNode arrayNode,ITNMElementNode childNode,boolean fireEvent,ITNMParams extendParams)
    {
        arrayNode.removeChild(childNode,fireEvent,extendParams);
        childNode.removeNodeEventListenser(arrayNode);
        return true;
    }
    
    public void unlinkAll(ITNMArrayNode arrayNode)
    {
        unlinkAll(arrayNode,true,new TNMParams());
    }
    
    public void unlinkAll(ITNMArrayNode arrayNode,boolean fireEvent)
    {
        unlinkAll(arrayNode,fireEvent,new TNMParams());
    }
        
    public void unlinkAll(ITNMArrayNode arrayNode,ITNMParams extendParams)
    {
        unlinkAll(arrayNode,true,extendParams);
    }
    
    public void unlinkAll(ITNMArrayNode arrayNode,boolean fireEvent,ITNMParams extendParams)
    {
        ITNMArrayList<ITNMElementNode> childNodes=arrayNode.getChildNodes();
        for(int i=0;i<childNodes.size();i++)
        {
            unlink(arrayNode,arrayNode,fireEvent,extendParams);
        }
    }
    
    private TNMNodeManager getNodeManager(String type)
    {
        TNMNodeManager nodeManager=nodeManagers.getParam(type);
        if(nodeManager==null)
        {
            nodeManager=new TNMNodeManager();
            nodeManagers.setParam(type,nodeManager);
        }
        return nodeManager;
    }
    
    public ITNMElementNode getNode(long id)
    {
        return getNode(TNMKJS.DEFAULT,id);
    }
    
    public ITNMElementNode getNode(String type,long id)
    {
        ITNMElementNode elementNode=getNodeManager(type).getNode(id);
        return elementNode;
    }
    
    public boolean addNode(ITNMElementNode elementNode)
    {
        String type=elementNode.getType();
        if(type==null)
            type=TNMKJS.DEFAULT;
        return getNodeManager(type).addNode(elementNode);
    }
    
    public ITNMElementNode removeNode(long id)
    {
        return removeNode(TNMKJS.DEFAULT,id);
    }
    
    public ITNMElementNode removeNode(String type,long id)
    {
        return getNodeManager(type).removeNode(id);
    }
    
    public long removeNode(ITNMElementNode elementNode)
    {
        String type=elementNode.getType();
        return getNodeManager(type).removeNode(elementNode);
    }
    
    public void onNewNotify(JsonObject data)
    {
        long nodeId=data.get(TNMKJS.NODE_ID).getAsLong();
        String nodeType=data.get(TNMKJS.TYPE).getAsString();
        String strEventType=data.get(TNMKJS.EVENT_TYPE).getAsString();
        JsonArray jaPaths=data.get(TNMKJS.PATH).getAsJsonArray();
        ITNMArrayList<String> paths=TNMFunction.convertJsonArrayToStringList(jaPaths);
        JsonElement valueJsonElement=data.get(TNMKJS.VALUE);
        TNMEventType eventType=TNMEventType.valueOf(strEventType);
        ITNMElementNode registerElementNode=getNode(nodeType,nodeId);
        if(registerElementNode==null)
            return ;
        
        if(paths.size()==0)
        {
            if(!eventType.equals(TNMEventType.UPDATED))
                return ;
            registerElementNode.replace(valueJsonElement);
            return ;
        }
        String lastField=paths.get(paths.size()-1);
        paths.remove(paths.size()-1);
        ITNMElementNode parentElementNode=registerElementNode.getLineage(paths);
        if(parentElementNode==null)
            return ;
        if(!(parentElementNode instanceof ITNMParentNode))
            return ;
        ITNMParentNode parentParentNode=(ITNMParentNode) parentElementNode;
        ITNMObjectNode parentObjectNode;
        ITNMArrayNode parentArrayNode;
        switch(eventType)
        {
            case UPDATED:
                ITNMElementNode childElementNode=null;
                if(parentParentNode instanceof ITNMObjectNode)
                {
                    parentObjectNode=(ITNMObjectNode) parentParentNode;
                    childElementNode=parentObjectNode.getChild(lastField);
                }
                if(parentParentNode instanceof ITNMArrayNode)
                {
                    parentArrayNode=(ITNMArrayNode) parentParentNode;
                    childElementNode=parentArrayNode.getChild(new Integer(lastField));
                }
                childElementNode.replace(valueJsonElement);
                break;
            case REMOVED:
                if(parentParentNode instanceof ITNMObjectNode)
                {
                    parentObjectNode=(ITNMObjectNode) parentParentNode;
                    unlink(lastField,parentObjectNode);
                }
                if(parentParentNode instanceof ITNMArrayNode)
                {
                    parentArrayNode=(ITNMArrayNode) parentParentNode;
                    unlink(new Integer(lastField), parentArrayNode);
                }
                break;
            case ADDED:
                ITNMElementNode newElementNode=null;
                if(valueJsonElement instanceof JsonPrimitive)
                    newElementNode=new TNMPrimitiveNode(valueJsonElement.getAsJsonPrimitive());
                if(valueJsonElement instanceof JsonObject)
                    newElementNode=new TNMObjectNode(valueJsonElement.getAsJsonObject());
                if(valueJsonElement instanceof JsonArray)
                    newElementNode=new TNMArrayNode(valueJsonElement.getAsJsonArray());
                if(parentParentNode instanceof ITNMObjectNode)
                {
                    parentObjectNode=(ITNMObjectNode) parentParentNode;
                    link(lastField, parentObjectNode,newElementNode);
                }
                if(parentParentNode instanceof ITNMArrayNode)
                {
                    parentArrayNode=(ITNMArrayNode) parentParentNode;
                    link(new Integer(lastField),parentArrayNode,newElementNode);
                }
                break;
        }
    }
}

class TNMNodeManager extends TNMObject
{
    private long maxId=0;
    private ITNMHashMap<Long,ITNMElementNode> nodeMap;
    
    public TNMNodeManager()
    {
        nodeMap=new TNMHashMap<Long,ITNMElementNode>();
    }
    
    synchronized public ITNMElementNode getNode(long id)
    {
        return nodeMap.get(id);
    }
    
    synchronized public boolean addNode(ITNMElementNode elementNode)
    {
        if(elementNode.getId()==0)
        {
            long newId=newId();
            elementNode.setId(newId);
        }
        nodeMap.put(elementNode.getId(),elementNode);
        return true;
    }
    
    synchronized public ITNMElementNode removeNode(long id)
    {
        return nodeMap.remove(id);
    }
    
    synchronized public long removeNode(ITNMElementNode elementNode)
    {
        return nodeMap.removeValue(elementNode);
    }
    
    private long newId()
    {
        maxId++;
        return maxId;
    }
}
