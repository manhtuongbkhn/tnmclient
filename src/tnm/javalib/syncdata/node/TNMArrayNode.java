package tnm.javalib.syncdata.node;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMParams;
import tnm.javalib.TNMArrayList;
import tnm.javalib.TNMParams;

public class TNMArrayNode extends TNMParentNode implements ITNMArrayNode
{
    protected ITNMArrayList<ITNMElementNode> childNodes;
    
    public TNMArrayNode(long id)
    {
        this(null,id,null);
    }
    
    public TNMArrayNode(String type,long id)
    {
        this(type,id,null);
    }
    
    public TNMArrayNode(String type,long id,String token)
    {
        this(type,id,token,null);
    }
    
    public TNMArrayNode(JsonArray jsonArray)
    {
        this(null,0,null,jsonArray);
    }
    
    protected TNMArrayNode(String type,long id,String token,JsonArray jsonArray)
    {
        super(type,id,token);
        childNodes=new TNMArrayList<ITNMElementNode>();
        if(jsonArray!=null)
            setContent(jsonArray);
    }
    
    @Override
    public void replace(JsonElement jsonElement)
    {
        replace(jsonElement,true,new TNMParams());
    }
    
    @Override
    public void replace(JsonElement jsonElement,boolean fireEvent)
    {
        replace(jsonElement,fireEvent,new TNMParams());
    }
    
    @Override
    public void replace(JsonElement jsonElement,ITNMParams extendParams)
    {
        replace(jsonElement,true,extendParams);
    }
    
    public void replace(JsonElement jsonElement,boolean fireEvent,ITNMParams extendParams)
    {
        if(jsonElement == null || (jsonElement instanceof JsonNull))
        {
            childNodes=null;
            if(fireEvent)
            {
                ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.UPDATED,this,extendParams,initMaxEventId());
                onChange(event);
            }
            return ;
        }
                
        if(jsonElement instanceof JsonArray)
        {
            JsonArray jsonArray=(JsonArray) jsonElement;
            setContent(jsonArray);
            if(fireEvent)
            {
                ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.UPDATED,this,extendParams,initMaxEventId());
                onChange(event);
            }
        }
        else
            throw new UnsupportedOperationException("Input must JsonArray");
    }
    
    private void setContent(JsonArray jsonArray)
    {
        if(childNodes==null)
            childNodes=new TNMArrayList<ITNMElementNode>();
        
        for(int i=0;i<jsonArray.size();i++)
        {
            JsonElement element=jsonArray.get(i);
            TNMElementNode elementNode=null;
            if(element.isJsonObject()||element.isJsonArray()||element.isJsonPrimitive())
            {
                if(element.isJsonObject())
                    elementNode=new TNMObjectNode(element.getAsJsonObject());
                if(element.isJsonArray())
                    elementNode=new TNMArrayNode(element.getAsJsonArray());
                if(element.isJsonPrimitive())
                    elementNode=new TNMPrimitiveNode(element.getAsJsonPrimitive());
                TNMNodeApi.getDefault().link(this,elementNode,false);
            }
        }
    }
    
    public ITNMElementNode getChild(int index)
    {
        return childNodes.get(index);
    }
    
    public int addChild(ITNMElementNode elementNode)
    {
        return addChild(elementNode,true,new TNMParams());
    }
    
    public int addChild(ITNMElementNode elementNode,boolean fireEvent)
    {
        return addChild(elementNode,fireEvent,new TNMParams());
    }
    
    public int addChild(ITNMElementNode elementNode,ITNMParams extendParams)
    {
        return addChild(elementNode,true,extendParams);
    }
    
    public int addChild(ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams)
    {   
        childNodes.add(elementNode);
        Integer index=childNodes.size()-1;
        if(fireEvent)
        {
            ITNMArrayList<String> paths=new TNMArrayList<String>();
            paths.add(index.toString());
            ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.ADDED,elementNode,paths,extendParams,initMaxEventId());
            onChange(event);
        }
        return index;
    }
    
        public int addChild(int index,ITNMElementNode elementNode)
    {
        return addChild(index,elementNode,true,new TNMParams());
    }
    
    public int addChild(int index,ITNMElementNode elementNode,boolean fireEvent)
    {
        return addChild(index,elementNode,fireEvent,new TNMParams());
    }
    
    public int addChild(int index,ITNMElementNode elementNode,ITNMParams extendParams)
    {
        return addChild(index,elementNode,true,extendParams);
    }
    
    public int addChild(int index,ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams)
    {
        childNodes.add(index,elementNode);
        if(fireEvent)
        {
            ITNMArrayList<String> paths=new TNMArrayList<String>();
            paths.add((new Integer(index)).toString());
            ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.ADDED,elementNode,paths,extendParams,initMaxEventId());
            onChange(event);
        }
        return index;
    }
    
    public ITNMElementNode removeChild(int index)
    {
        return removeChild(index,true);
    }
    
    public ITNMElementNode removeChild(int index,boolean fireEvent)
    {
        return removeChild(index,fireEvent,new TNMParams());
    }
    
    public ITNMElementNode removeChild(int index,ITNMParams extendParams)
    {
        return removeChild(index,true,extendParams);
    }
    
    public ITNMElementNode removeChild(int index,boolean fireEvent,ITNMParams extendParams)
    {
        ITNMElementNode elementNode=childNodes.remove(index);
        if(fireEvent)
        {
            ITNMArrayList<String> paths=new TNMArrayList<String>();
            paths.add(new Integer(index).toString());
            ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.REMOVED,elementNode,paths,extendParams,initMaxEventId());
            onChange(event);
        }
        return elementNode;
    }

    public boolean removeChild(ITNMElementNode elementNode)
    {
        return removeChild(elementNode,true,new TNMParams());
    }
    
    public boolean removeChild(ITNMElementNode elementNode,boolean fireEvent)
    {
        return removeChild(elementNode,fireEvent,new TNMParams());
    }
    
    public boolean removeChild(ITNMElementNode elementNode,ITNMParams extendParams)
    {
        return removeChild(elementNode,true,extendParams);
    }
    
    public boolean removeChild(ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams)
    {
        for(int index=0;index<childNodes.size();index++)
        {
            ITNMElementNode currentElementNode=childNodes.get(index);
            if(currentElementNode==elementNode)
            {
                childNodes.remove(index);
                if(fireEvent)
                {
                    ITNMArrayList<String> paths=new TNMArrayList<String>();
                    paths.add(new Integer(index).toString());
                    ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.REMOVED,elementNode,paths,extendParams,initMaxEventId());
                    onChange(event);
                }
                return true;
            }
        }
        return false;
    }
    
    @Override
    public JsonElement toJson()
    {
        JsonArray jsonArray=new JsonArray();
        for(int i=0;i<childNodes.size();i++)
        {
            ITNMElementNode elementNode=childNodes.get(i);
            jsonArray.add(elementNode.toJson());
        }
        return jsonArray;
    }

    public boolean isNull()
    {
        if(childNodes==null)
            return true;
        else
            return false;
    }
    
    @Override
    public int getChildNodeCount() 
    {
        return childNodes.size();
    }
    
    @Override
    public ITNMArrayList<ITNMElementNode> getChildNodes()
    {
        return childNodes;
    }

    @Override
    protected String getKey(ITNMElementNode node) 
    {
        Integer index=childNodes.indexOf(node);
        return index.toString();
    }
}
