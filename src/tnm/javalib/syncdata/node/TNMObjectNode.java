package tnm.javalib.syncdata.node;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.Map;
import tnm.javalib.ITNMArrayList;
import tnm.javalib.ITNMParams;
import tnm.javalib.TNMArrayList;
import tnm.javalib.TNMParams;

public class TNMObjectNode extends TNMParentNode implements ITNMObjectNode
{
    protected ITNMParams<ITNMElementNode> childNodes;
    
    public TNMObjectNode()
    {
        this(null,0,null);
    }
    
    public TNMObjectNode(String type,long id)
    {
        this(type,id,"",null);
    }
    
    public TNMObjectNode(String type,long id,String token)
    {
        this(type,id,token,null);
    }
    
    public TNMObjectNode(JsonObject jsonObject)
    {
        this(null,0,null,jsonObject);
    }
    
    protected TNMObjectNode(String type,long id,String token,JsonObject jsonObject)
    {
        super(type,id,token);
        childNodes=new TNMParams<ITNMElementNode>();
        if(jsonObject!=null)
            setContent(jsonObject);
    }
    
    @Override
    public void replace(JsonElement jsonElement)
    {
        replace(jsonElement,true,new TNMParams());
    }
    
    @Override
    public void replace(JsonElement jsonElement,boolean fireEvent)
    {
        replace(jsonElement,fireEvent,new TNMParams());
    }
    
    @Override
    public void replace(JsonElement jsonElement,ITNMParams extendParams)
    {
        replace(jsonElement,true,extendParams);
    }
    
    @Override
    public void replace(JsonElement jsonElement ,boolean fireEvent,ITNMParams extendParams)
    {
        if(jsonElement == null || (jsonElement instanceof JsonNull))
        {
            childNodes=null;
            if(fireEvent)
            {
                ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.UPDATED,this,extendParams,initMaxEventId());
                onChange(event);
            }
            return ;
        }
        
        if(jsonElement instanceof JsonObject)
        {
            JsonObject jsonObject=(JsonObject) jsonElement;
            setContent(jsonObject);
            if(fireEvent)
            {
                ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.UPDATED,this,extendParams,initMaxEventId());
                onChange(event);
            }
        }
        else
            throw new UnsupportedOperationException("Input must JsonObject");
    }
    
    private void setContent(JsonObject jsonObject)
    {
        if(childNodes==null)
            childNodes=new TNMParams<ITNMElementNode>();
        Iterator<Map.Entry<String,JsonElement>> iterator=jsonObject.entrySet().iterator();
        while(iterator.hasNext())
        {
            Map.Entry<String,JsonElement> entry=iterator.next();
            String key=entry.getKey();
            JsonElement element=entry.getValue();
            TNMElementNode elementNode=null;
            if(element.isJsonObject()||element.isJsonArray()||element.isJsonPrimitive())
            {
                if(element.isJsonObject())
                    elementNode=new TNMObjectNode(element.getAsJsonObject());
                if(element.isJsonArray())
                    elementNode=new TNMArrayNode(element.getAsJsonArray());
                if(element.isJsonPrimitive())
                    elementNode=new TNMPrimitiveNode(element.getAsJsonPrimitive());
                TNMNodeApi.getDefault().link(key,this,elementNode,false);
            }
        }
    }
    
    public ITNMElementNode getChild(String field)
    {
        return childNodes.getParam(field);
    } 
    
    public boolean addChild(String field,ITNMElementNode elementNode,boolean fireEvent)
    {
        return addChild(field,elementNode,fireEvent,new TNMParams());
    }
    
    public boolean addChild(String field,ITNMElementNode elementNode,ITNMParams extendParams)
    {
        return addChild(field,elementNode,true,extendParams);
    }
    
    public boolean addChild(String field,ITNMElementNode elementNode)
    {
        return addChild(field,elementNode,true,new TNMParams());
    }
    
    public boolean addChild(String field,ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams)
    {
        childNodes.setParam(field,elementNode);
        if(fireEvent)
        {
            ITNMArrayList<String> paths=new TNMArrayList<String>();
            paths.add(field);
            ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.ADDED,elementNode,paths,extendParams,initMaxEventId());
            onChange(event);
        }
        return true;
    }
    
    public ITNMElementNode removeChild(String field,ITNMParams exetendParams)
    {
        return removeChild(field,true,exetendParams);
    }
    
    public ITNMElementNode removeChild(String field,boolean fireEvent)
    {
        return removeChild(field,fireEvent,new TNMParams());
    }
    
    public ITNMElementNode removeChild(String field)
    {
        return removeChild(field,true,new TNMParams());
    }
    
    public ITNMElementNode removeChild(String field,boolean fireEvent,ITNMParams extendParams)
    {
        ITNMElementNode elementNode=childNodes.removeParam(field);
        if(fireEvent)
        {
            ITNMArrayList<String> paths=new TNMArrayList<String>();
            paths.add(field);
            ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.REMOVED,elementNode,paths,extendParams,initMaxEventId());
            onChange(event);
        }
        return elementNode;
        
    }
    
    public String removeChild(ITNMElementNode elementNode,boolean fireEvent)
    {
        return removeChild(elementNode,fireEvent,new TNMParams());
    }
    
    public String removeChild(ITNMElementNode elementNode,ITNMParams extendParams)
    {
        return removeChild(elementNode,true,extendParams);
    }
    
    public String removeChild(ITNMElementNode elementNode)
    {
        return removeChild(elementNode,true,new TNMParams());
    }
    
    public String removeChild(ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams)
    {
        String field=childNodes.removeParam(elementNode);
        if(fireEvent)
        {
            ITNMArrayList<String> paths=new TNMArrayList<String>();
            paths.add(field);
            ITNMNodeEvent event=new TNMNodeEvent(TNMEventType.REMOVED,elementNode,paths,extendParams,initMaxEventId());
            onChange(event);
        }
        return field;
    }
    
    @Override
    public JsonElement toJson()
    {
        JsonObject jsonObject=new JsonObject();
        Iterator<String> iterator=childNodes.getKeys();
        while(iterator.hasNext())
        {
            String field=iterator.next();
            ITNMElementNode elementNode=childNodes.getParam(field);
            jsonObject.add(field,elementNode.toJson());
        }
        return jsonObject;
    }

    public boolean isNull()
    {
        if(childNodes==null)
            return true;
        else
            return false;
    }
    
    @Override
    public int getChildNodeCount() 
    {
        return childNodes.getParamCount();
    }
    
    @Override
    public Iterator<String> getFields()
    {
        return childNodes.getKeys();
    }
    
    @Override
    protected String getKey(ITNMElementNode node) 
    {
        return childNodes.getKey(node);
    }
}
