package tnm.javalib.syncdata.node; 

import java.util.Iterator;
import tnm.javalib.ITNMParams;

public interface ITNMObjectNode extends ITNMParentNode
{           
    public ITNMElementNode getChild(String field);
    
    public boolean addChild(String field,ITNMElementNode elementNode);
    
    public boolean addChild(String field,ITNMElementNode elementNode,boolean fireEvent);
    
    public boolean addChild(String field,ITNMElementNode elementNode,ITNMParams extendParams);
    
    public boolean addChild(String field,ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams);
    
    public ITNMElementNode removeChild(String field);
    
    public ITNMElementNode removeChild(String field,boolean fireEvent);
    
    public ITNMElementNode removeChild(String field,ITNMParams extendParams);
    
    public ITNMElementNode removeChild(String field,boolean fireEvent,ITNMParams extendParams);
    
    public String removeChild(ITNMElementNode elementNode);
    
    public String removeChild(ITNMElementNode elementNode,boolean fireEvent);
    
    public String removeChild(ITNMElementNode elementNode,ITNMParams extendParams);
    
    public String removeChild(ITNMElementNode elementNode,boolean fireEvent,ITNMParams extendParams);
    
    public Iterator<String> getFields();
}
