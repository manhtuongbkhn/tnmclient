 package tnm.javalib.syncdata;

import tnm.javalib.ITNMParams;
import tnm.javalib.TNMObject;
import tnm.javalib.TNMParams;

public class TNMSyncConfig extends TNMObject
{
    private static ITNMParams<TNMSyncConfig> configs=new TNMParams<TNMSyncConfig>();
    
    public static void setDefault(TNMSyncConfig config)
    {
        setInstance(null,config);
    }
    
    public static void setInstance(String name,TNMSyncConfig config)
    {
        if(name==null)
            name="default";
        if(configs.getParam(name)==null)
            configs.setParam(name,config);
    }
    
    public static TNMSyncConfig getDefault()
    {
        return getInstance(null);
    }
    
    public static TNMSyncConfig getInstance(String name)
    {
        if(name==null)
            name="default";
        TNMSyncConfig config=configs.getParam(name);
        if(config==null)
        {
            config=new TNMSyncConfig();
            configs.setParam(name,config);
        }
        return config;
    }
    
    public final int EVENT_HISTORY_COUNT=20;
}
