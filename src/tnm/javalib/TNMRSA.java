package tnm.javalib;

import com.google.gson.Gson;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

public class TNMRSA extends TNMObject implements ITNMRSA
{
    private PublicKey publicKey;
    private PrivateKey privateKey;
    
    public TNMRSA()
    {
    }
    
    public ITNMResult init(String strPublicKey,String strPrivateKey)
    {    
        try 
        {
            byte[]byteArrPublicKey=TNMFunction.convertStringCodeToByteArrayCode(strPublicKey);
            publicKey=KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(byteArrPublicKey));
            byte[]byteArrPrivateKey=TNMFunction.convertStringCodeToByteArrayCode(strPrivateKey);
            privateKey=KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(byteArrPrivateKey));
            return new TNMResult(0,"Ok",this);
        } 
        catch (Exception ex) 
        {
            return new TNMResult(1,ex.getMessage(),null);
        }
    }
    
    public ITNMResult encrypt(String message)
    {
        try 
        {
            Cipher cipher=Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE,publicKey);
            Gson gson=new Gson();
            String encryptMessage=TNMFunction.convertByteArrayCodeToStringCode
                                            (cipher.doFinal(message.getBytes()));
            return new TNMResult(0,"Ok",encryptMessage);
        } 
        catch (Exception ex)
        {
            return new TNMResult(1,ex.getMessage(),null);
        }
    }
    
    public ITNMResult decrypt(String codeMessage)
    {
        try 
        {
            Cipher cipher=Cipher.getInstance("RSA/ECB/PKCS1Padding");   
            cipher.init(Cipher.DECRYPT_MODE,privateKey);
            String decryptMessage=new String(cipher.doFinal(TNMFunction
                            .convertStringCodeToByteArrayCode(codeMessage)));
            return new TNMResult(0,"Ok",decryptMessage);
        } 
        catch (Exception ex)
        {
            return new TNMResult(1,ex.getMessage(),null);
        }
    }
}
