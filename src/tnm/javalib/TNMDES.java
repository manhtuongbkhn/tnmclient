package tnm.javalib;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class TNMDES extends TNMObject implements ITNMDES
{
    public TNMDES()
    {
        
    }
    
    public ITNMResult encrypt(String StrEncode,String message)
    {
        try 
        {
            Cipher cipher=Cipher.getInstance("DES");
            SecretKeySpec secretKeySpec=new SecretKeySpec
                (TNMFunction.convertStringCodeToByteArrayCode(StrEncode),"DES");
            cipher.init(Cipher.ENCRYPT_MODE,secretKeySpec);
            byte[] byteArrEncrytMessage = cipher.doFinal(message.getBytes("UTF8"));
            String encryptMessage=TNMFunction.convertByteArrayCodeToStringCode(byteArrEncrytMessage);
            return new TNMResult(0,"Ok",encryptMessage);
        } 
        catch (Exception ex) 
        {
            return new TNMResult(1,ex.getMessage(),null);
        }
    }

    public ITNMResult decrypt(String StrEncode,String encrytMessage) 
    { 
        try 
        {
            Cipher cipher=Cipher.getInstance("DES");
            SecretKeySpec keySpec=new SecretKeySpec
                (TNMFunction.convertStringCodeToByteArrayCode(StrEncode),"DES");
            cipher.init(Cipher.DECRYPT_MODE,keySpec);
            byte[] byteArrDecryptMessage=cipher.doFinal
                (TNMFunction.convertStringCodeToByteArrayCode(encrytMessage));
            String decryptMessage=new String(byteArrDecryptMessage,"UTF8");
            return new TNMResult(0,"Ok",decryptMessage);
        } 
        catch (Exception ex) 
        {
            return new TNMResult(1,ex.getMessage(),null);
        }
    }
    
    public ITNMResult createKey()
    {
        try 
        {
            KeyGenerator keyGenerator=KeyGenerator.getInstance("DES");
            keyGenerator.init(56,new SecureRandom());
            SecretKey secretKey = keyGenerator.generateKey();
            String key=TNMFunction.convertByteArrayCodeToStringCode(secretKey.getEncoded());
            return new TNMResult(0,"Ok",key);
        } 
        catch (Exception ex) 
        {
            return new TNMResult(1,ex.getMessage(),null);
        }
    }
    
    public boolean checkKey(String desKey)
    {
        return true;
    }
}
