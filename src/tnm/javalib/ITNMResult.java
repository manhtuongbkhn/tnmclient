package tnm.javalib;

public interface ITNMResult 
{
    public boolean isSuccess();
    
    public int getCode();
    
    public String getMessage();
    
    public Object getResult();
    
    public void setCode(int code);
    
    public void setMessage(String message);

    public void setResult(Object result);
}
