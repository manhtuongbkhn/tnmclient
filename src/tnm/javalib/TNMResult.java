package tnm.javalib;

public class TNMResult implements ITNMResult
{
    private static final int successCode=0;
    protected int code;
    protected String message;
    protected Object result;
    
    public TNMResult()
    {
        
    }
    
    public TNMResult(int code,String message,Object result)
    {
        this.code=code;
        this.message=message;
        this.result=result;
    }
    
    public boolean isSuccess()
    {
        return code==successCode;
    }
    
    public int getCode()
    {
        return code;
    }
    
    public String getMessage()
    {
        return message;
    }
    
    public Object getResult()
    {
        return result;
    }

    public void setCode(int code) 
    {
        this.code = code;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public void setResult(Object result) 
    {
        this.result = result;
    }
}
