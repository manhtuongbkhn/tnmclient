package tnm.javalib;

import java.util.Iterator;

public interface ITNMParams<T>
{
    public int addParam(T value);
    
    public boolean setParam(String key,T value);
    
    public T removePamram(int index);
    
    public T removeParam(String key);
    
    public String removeParam(T t);
    
    public T getParam(String key);
    
    public String getKey(T t);
    
    public Iterator<String> getKeys();
    
    public boolean containsKey(String key);
    
    public void clear();
    
    public int getParamCount();
    
    public String showContent();
    
    public ITNMParams<T> clone();
}
