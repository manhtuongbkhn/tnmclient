package tnm.javalib;

import java.util.List;
import java.util.RandomAccess;

public interface ITNMArrayList<T> extends List<T>, RandomAccess, Cloneable, java.io.Serializable
{
    public Object clone();
}
