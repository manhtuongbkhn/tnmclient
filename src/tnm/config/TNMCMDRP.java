package tnm.config;

import tnm.javalib.TNMObject;

public class TNMCMDRP extends TNMObject
{
    public static final String LAST_RESPONSE="_response";
    public static final String TNM_COMMUNICATE="tnm_communicate";
    public static final String TNM_CREATE_TOKEN="tnm_create_token";
    public static final String TNM_REGISTER_NODE="tnm_register_node";
    public static final String TNM_UNREGISTER_NODE="tnm_unregister_node";
}
