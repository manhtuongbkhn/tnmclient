package tit;

import com.google.gson.JsonObject;
import tnm.error.ITNMError;
import tnm.javalib.ITNMParams;
import tnm.message.ITNMResponse;
import tnm.TNMClient;
import tnm.javalib.syncdata.node.ITNMElementNode;
import tnm.javalib.syncdata.node.ITNMNodeEvent;
import tnm.javalib.syncdata.node.ITNMNodeEventListener;
import tnm.javalib.syncdata.node.ITNMObjectNode;
import tnm.javalib.syncdata.node.TNMObjectNode;
import tnm.config.TNMConfig;
import tnm.javalib.TNMKJS;
import tnm.message.ITNMNotify;
import tnm.message.ITNMRequest;
import tnm.message.ITNMRequestHandler;
import tnm.message.ITNMSMessage;
import tnm.message.TNMRequest;

public class TITClient extends TNMClient
{
    public TITClient(String host, int port) 
    {
        super(host,port,new TNMConfig());
    }
    
    @Override
    public void handling(ITNMSMessage sMessage) 
    {
        
    }

    @Override
    public void onHandshaked()
    {
        System.out.println("On Handshaked");
        requestUserInfo();
    }
    
    protected void requestUserInfo()
    {
        JsonObject data=new JsonObject();
        data.addProperty("name","Nguyen Manh Tuong");
        ITNMRequest request=new TNMRequest("api.RegisterUserInfo",data);
        ITNMRequestHandler requestHandler=new ITNMRequestHandler() 
        {
            @Override
            public void onRequestSuccess(ITNMRequest request,ITNMResponse response) 
            {
                if(response.getData().get(TNMKJS.SUCCESS).getAsBoolean())
                {
                    long noded=response.getData().get(TNMKJS.NODE_ID).getAsInt();
                    String noteType=response.getData().get(TNMKJS.NODE_TYPE).getAsString();
                    registerUserInfo(noteType,noded);
                }
            }

            @Override
            public void onRequestError(ITNMRequest request,int code, String message)
            {
                
            }
        };
        request.addRequestHandler(requestHandler);
        sendRequest(request);
    }
    
    protected void registerUserInfo(String nodeType,long nodeId)
    {
        final ITNMObjectNode objectNode=new TNMObjectNode(nodeType,nodeId);
        objectNode.addNodeEventListenser(new ITNMNodeEventListener() 
        {
            protected boolean loaded=false;
            
            @Override
            public void onListenNodeChange(ITNMElementNode elementNode, ITNMNodeEvent event) 
            {
                switch(event.getEventType())
                {
                    case ADDED:
                        break;
                    case REMOVED:
                        break;
                    case UPDATED:
                        if(!this.loaded)
                        {
                            this.loaded=true;
                            requestJoinRoom();
                        }
                        break;
                }
                System.out.println(event.getEventType());
                System.out.println(event.getPaths());
                System.out.println(objectNode.toJson());
            }

            @Override
            public void onListenNodeError(int code, String message, ITNMParams params)
            {
                
            }
        });
        registerNode(objectNode);
    }
    
    protected void requestJoinRoom()
    {
        JsonObject data=new JsonObject();
        data.addProperty(TNMKJS.ROOM_ID,1);
        ITNMRequest joinRoomRequest=new TNMRequest("api.JoinRoom",data);    
        ITNMRequestHandler requestHandler=new ITNMRequestHandler() 
        {
            @Override
            public void onRequestSuccess(ITNMRequest request,ITNMResponse response) 
            {
                if(response.getData().get(TNMKJS.SUCCESS).getAsBoolean())
                {
                    int roomId=request.getData().get(TNMKJS.ROOM_ID).getAsInt();
                    //requestRoomInfo(roomId);
                }
            }
            
            @Override
            public void onRequestError(ITNMRequest request,int code, String message) 
            {
                
            }
        };
        joinRoomRequest.addRequestHandler(requestHandler);
        sendRequest(joinRoomRequest);
    }
    
    protected void requestRoomInfo(int roomId)
    {
        JsonObject data=new JsonObject();
        data.addProperty(TNMKJS.ROOM_ID,roomId);
        
        ITNMRequest request=new TNMRequest("api.RegisterRoomInfo", data);
        ITNMRequestHandler requestHandler=new ITNMRequestHandler() 
        {
            @Override
            public void onRequestSuccess(ITNMRequest request,ITNMResponse response) 
            {
                if(response.getData().get(TNMKJS.SUCCESS).getAsBoolean())
                {
                    String noteType=response.getData().get(TNMKJS.NODE_TYPE).getAsString();
                    long nodeId=response.getData().get(TNMKJS.NODE_ID).getAsInt();
                    registerRoomInfo(noteType,nodeId);
                }
            }
            
            @Override
            public void onRequestError(ITNMRequest request,int code, String message) 
            {
                
            }
        };
        request.addRequestHandler(requestHandler);
        sendRequest(request);
    }
    
    protected void registerRoomInfo(String nodeType,long nodeId)
    {
        final ITNMObjectNode objectNode=new TNMObjectNode(nodeType,nodeId);
        objectNode.addNodeEventListenser(new ITNMNodeEventListener() 
        {
            protected boolean loaded=false;
            
            @Override
            public void onListenNodeChange(ITNMElementNode elementNode, ITNMNodeEvent event) 
            {
                switch(event.getEventType())
                {
                    case ADDED:
                        System.out.println("ADDED");
                        System.out.println(objectNode.toJson());
                        break;
                    case REMOVED:
                        System.out.println("REMOVE");
                        System.out.println(objectNode.toJson());
                        break;
                    case UPDATED:
                        System.out.println("UPDATE");
                        System.out.println(objectNode.toJson());
                        if(!this.loaded)
                        {
                            this.loaded=true;                           
                        }
                        break;
                }
            }

            @Override
            public void onListenNodeError(int code, String message, ITNMParams params) 
            {
                
            }
        });
        registerNode(objectNode);
    }
    
    protected void changeName()
    {
        JsonObject data=new JsonObject();
        data.addProperty("name","Nguyen Thuy Nga");
        ITNMRequest changeNameRequest=new TNMRequest("api.ChangeName",data);
        
        ITNMRequestHandler requestHandler=new ITNMRequestHandler() 
        {
            @Override
            public void onRequestSuccess(ITNMRequest request, ITNMResponse response)
            {
                
            }

            @Override
            public void onRequestError(ITNMRequest request, int code, String message) 
            {
                
            }
        };
        changeNameRequest.addRequestHandler(requestHandler);
        sendRequest(changeNameRequest);
    }
    
    @Override
    protected void error(ITNMError error, ITNMParams params) 
    {
        System.out.println("Code: "+error.getCode());
        System.out.println("Message: "+error.getMessage());
        if(params.getParam(TNMKJS.EXCEPTION)!=null)
        {
            Exception ex=(Exception)params.getParam(TNMKJS.EXCEPTION);
            ex.printStackTrace();
        }
        Thread.currentThread().stop();
    }
    
    @Override
    public boolean handling(ITNMResponse response) 
    {
        return false;
    }

    @Override
    public void handling(ITNMNotify notify) 
    {
        
    }
}
